# README #

Feel free to use this library if you give me (blootooby) credit for writing this library. :P

### What is this repository for? ###

This is basically a library for **basic** Java deobfuscation. It's made for use with games like Minecraft, but it can be used for other applications too.
You need ASM for this... and I think that's all.

### Who do I talk to? ###

* blootooby

### What does [x] do? ###

Take a look for yourself, the source is literally right here.

### How the fuck do I use this shit? ###

Okay, so you do something like this:


```
#!java

Collection<String> classesToSearch = new HashSet<String>();

Enumeration<JarEntry> entries = minecraftJar.entries();
while (entries.hasMoreElements()) {
  JarEntry en = entries.nextElement();
  if (en.isDirectory() || !en.getName().endsWith(".class")) {
    continue;
  }
  classesToSearch.add(en.getName().replace('/', '.').substring(0, en.getName().length() - 6));
}

MapperManager mapManager = new MapperManager();
mapManager.addMapper(new Mapper("Minecraft", "net.minecraft.client.main.Main"){
  @Override
  public void run(Collection<String> classesToSearch, Map<String, ClassNode> classMap,
      Map<String, FieldNode> fieldMap, Map<String, MethodNode> methodMap) throws Exception {
    ClassNode main = classMap.get(prerequisites[0]);
    Collection<String> mainRefs = MappingUtils.getReferencedClasses(main);
    mainRefs.retainAll(classesToSearch);
    for (String s : mainRefs) {
      try {
        ClassNode cn = new ClassNode(Opcodes.ASM4);
        MappingUtils.getClassReaderFromJar(minecraftJar, s).accept(cn, 0);
        if (MappingUtils.getReferencedClasses(cn).contains("org.lwjgl.opengl.Display")) {
          classMap.put(deobfuscatedName, cn);
          String x = 'L' + cn.name + ';';
          int access = Modifier.PRIVATE | Modifier.STATIC;
          for (FieldNode fn : MappingUtils.getFields(cn)) {
            if (access == fn.access && x.equals(fn.desc)) {
              fieldMap.put(deobfuscatedName + ".theMinecraft", fn);
            }
          }

          return;
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public void addSubMappers() {
    this.myManager.addMapper(new Mapper(this.deobfuscatedName + ".session",
        this.deobfuscatedName, "Session") {

      @Override
      public void run(Collection<String> classesToSearch, Map<String, ClassNode> classMap,
          Map<String, FieldNode> fieldMap, Map<String, MethodNode> methodMap) throws Exception {
        int access = Modifier.PRIVATE | Modifier.FINAL;
        ClassNode mc = classMap.get(prerequisites[0]);
        ClassNode session = classMap.get(prerequisites[1]);
        String desc = 'L' + session.name + ';';
        for (FieldNode fn : MappingUtils.getFields(mc)) {
          if (fn != null && fn.access == access && desc.equals(fn.desc)) {
            fieldMap.put(deobfuscatedName, fn);
            return;
          }
        }
      }
    });
});
mapManager.runMappers(classesToSearch);

System.out.println("Dumping class mappings...");
{
  List<String> alpha = new ArrayList<String>();
  alpha.addAll(mapManager.classMappings.keySet());
  Collections.sort(alpha);
  for (String s : alpha) {
    System.out.println("\t" + s + " maps to "
        + mapManager.classMappings.get(s).name);
  }
}
System.out.println();

System.out.println("Dumping method mappings...");
{
  List<String> alpha = new ArrayList<String>();
  alpha.addAll(mapManager.methodMappings.keySet());
  Collections.sort(alpha);
  for (String s : alpha) {
    System.out.println("\t" + s + " maps to "
        + mapManager.methodMappings.get(s).name);
  }
}
System.out.println();
System.out.println("Dumping field mappings...");
{
  List<String> alpha = new ArrayList<String>();
  alpha.addAll(mapManager.fieldMappings.keySet());
  Collections.sort(alpha);
  for (String s : alpha) {
    System.out.println("\t" + s + " maps to "
        + mapManager.fieldMappings.get(s).name);
  }
}
```

As for the hook package, at the moment a hook is created like this (though this may be subject to change):

```
#!java

@Reflected(className = "World")
public interface World extends Hook {
  @ExistingMethod(name = "getTopBlock", descriptor = "(LBlockPosition;)LBlock;")
  public Object getTopBlock(Object world, Object pos);
  
  @ExistingMethod(name = "setBlock", descriptor = "(LBlockPosition;LBlock;)V")
  public void setBlock(Object world, Object pos, Object block);
  
  @ExistingMethod(name = "getChunkFromBlockCoords", descriptor = "(LBlockPosition;)LChunk;")
  public void getChunkFromBlockCoords(Object world, Object blockPosition);

  @ExistingMethod(name = "getChunkFromChunkCoords", descriptor = "(II)LChunk;")
  public void getChunkFromChunkCoords(Object world, int chunkX, int chunkY);
}

```

Hooks can also have getters and setters for fields:

```
#!java

@Reflected(className = "GameSettings")
public interface GameSettings extends Hook {

  @Getter(fieldName = "gamma")
  public float getGamma(Object gs);
  
  @Setter(fieldName = "gamma")
  public void setGamma(Object gs, float gamma);
  
  @Getter(fieldName = "guiScale")
  public int getGuiScale(Object gs);
  
  @Setter(fieldName = "guiScale")
  public void setGuiScale(Object gs, int guiScale);
  
  @ExistingMethod(name = "saveOptions", descriptor = "()V")
  public void saveOptions(Object gs);
}

```


And the hook is instantiated like this:

```
#!java

HookFactory.createHook(worldClass, World.class, mapManager);

World world = HookFactory.getHook(World.class);
```