package nf.co.haxter.reflected.interfacer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface CustomMethod {
  public Class<? extends Runnable> codeDefinition(); // back in my day, we didn't have fucking lambdas and shit. :|
}
