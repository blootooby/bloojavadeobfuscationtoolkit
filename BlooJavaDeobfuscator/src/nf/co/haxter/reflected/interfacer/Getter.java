package nf.co.haxter.reflected.interfacer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author blootooby
 * @since Aug 1, 2014
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Getter {
  public String fieldName();
  public boolean isObfuscated() default true;
}
