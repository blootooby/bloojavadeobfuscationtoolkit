package nf.co.haxter.reflected.interfacer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ExistingMethod {
  public boolean isObfuscated() default true;
  public boolean isStatic() default false;
  public String name();
  public String descriptor();
}
