package nf.co.haxter.reflected;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author blootooby
 * @since July 29, 2014
 */
public abstract class Reflector {
  private Class<?> target;

  protected Reflector(Class<?> targetClass) {
    this.target = targetClass;
    for (Method m : targetClass.getDeclaredMethods()) {
      m.setAccessible(true);
    }
    for (Field f : targetClass.getDeclaredFields()) {
      f.setAccessible(true);
    }
    for (Constructor<?> c : targetClass.getDeclaredConstructors()) {
      c.setAccessible(true);
    }
  }

  /**
   * @return the class this reflector is targeting.
   */
  protected Class<?> getTarget() {
    return target;
  }

  /**
   * Finds the most appropriate method to invoke based on given arguments.
   * 
   * @param methodName the name of the method being looked up.
   * @param isStatic whether the method is static.
   * @param args the arguments given to the method.
   * @return the method that was most appropriate for the given arguments, or null if none was
   *         found.
   * @see Method
   * @see Modifier
   * @see Class#isAssignableFrom(Class)
   * @see Modifier#isStatic(int)
   * @see Modifier#STATIC
   */
  protected final Method lookupAppropriate(String methodName, Object... args) {
    Method[] all = target.getDeclaredMethods();
    if (all.length == 1) {
      return all[0];
    }
    for (Method m : all) {
      if (m.getName().equals(methodName)) {
        Class<?>[] paramTypes = m.getParameterTypes();
        if (paramTypes.length == args.length) {
          boolean works = true;
          for (int i = 0; i < paramTypes.length; i++) {
            if (!paramTypes[i].isAssignableFrom(args[i].getClass())) {
              works = false;
              break;
            }
          }
          if (works) {
            return m;
          }
        }
      }
    }
    return null;
  }
  
  public Object invoke(Object instance, String name, Object...args) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    Method m = this.lookupAppropriate(name, args);
    if (m == null) {
      System.err.println("Requested method was null:");
      System.err.println(String.format("Name: %s, Instance: %s", name, instance));
      
      for (Object o : args) {
        System.out.println(o.getClass().getName());
      }
      for (Method m1 : this.target.getDeclaredMethods()) {
        System.out.println(String.format("%s %s(%s)%s", Modifier.toString(m1.getModifiers()), m1.getName(), Arrays.toString(m1.getParameterTypes()), m1.getReturnType()));
      }
    }
    
    return m.invoke(instance, args);
  }


  public Field getField(String name) throws NoSuchFieldException, SecurityException {
    Field f = this.target.getDeclaredField(name);
    f.setAccessible(true);
    return f;
  }

  public Constructor getConstructor(Class<?>[] paramTypes) throws NoSuchMethodException,
      SecurityException {
    return this.target.getDeclaredConstructor(paramTypes);
  }

  public Constructor getAppropriateConstructor(Object... args) {
    for (Constructor c : this.target.getDeclaredConstructors()) {
      Class<?>[] params = c.getParameterTypes();
      if (params.length == args.length) {
        boolean works = true;
        for (int i = 0; i < params.length; i++) {
          Class<?> argCl = args[i].getClass();
          if (!params[i].isAssignableFrom(argCl)) {
            if (argCl.equals(Integer.class)) {
              argCl = Integer.TYPE;
            } else if (argCl.equals(Long.class)) {
              argCl = Long.TYPE;
            } else if (argCl.equals(Float.class)) {
              argCl = Float.TYPE;
            } else if (argCl.equals(Double.class)) {
              argCl = Double.TYPE;
            } else if (argCl.equals(Byte.class)) {
              argCl = Byte.TYPE;
            } else if (argCl.equals(Short.class)) {
              argCl = Short.TYPE;
            } else if (argCl.equals(Character.class)) {
              argCl = Character.TYPE;
            } else if (argCl.equals(Boolean.class)) {
              argCl = Boolean.TYPE;
            } else {
              works = false;
              break;
            }
            if (!params[i].isAssignableFrom(argCl)) {
              works = false;
              break;
            }
          }
        }
        if (works) {
          return c;
        }
      }
    }
    return null;
  }
  
  public Object instantiate(Object...args) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    return getAppropriateConstructor(args).newInstance(args);
  }
  
  public boolean verifyInstance(Object object) {
    return this.target.isAssignableFrom(object.getClass());
  }



  public static final class MethodInfo {

    private final String name;
    private final Class<?>[] paramTypes;

    private MethodInfo(String name, Class<?>[] paramTypes) {
      this.name = name;
      this.paramTypes = paramTypes;
    }

    public boolean equals(Method method) {
      if (method.getName().equals(name)) {
        Class<?>[] params = method.getParameterTypes();
        if (params.length != this.paramTypes.length) {
          return false;
        }
        for (int i = 0; i < params.length; i++) {
          if (!params[i].equals(this.paramTypes[i])) {
            return false;
          }
        }
        return true;
      }
      return false;
    }
  }
}
