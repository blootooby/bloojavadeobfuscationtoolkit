package nf.co.haxter.reflected;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map.Entry;

import nf.co.haxter.mapper.MapperManager;
import nf.co.haxter.reflected.interfacer.Constructor;
import nf.co.haxter.reflected.interfacer.CustomMethod;
import nf.co.haxter.reflected.interfacer.ExistingMethod;
import nf.co.haxter.reflected.interfacer.Getter;
import nf.co.haxter.reflected.interfacer.Reflected;
import nf.co.haxter.reflected.interfacer.Setter;
import nf.co.haxter.util.BytecodeUtils;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.util.CheckClassAdapter;

/**
 * @author blootooby
 * @since Aug 1, 2014
 */
public class ReflectorGenerator {
  private static String reflectorClassName = Reflector.class.getName().replace('.', '/');

  private ClassWriter cip; // Class In Progress
  private Class<?> hookInterface;
  private MapperManager mapManager;
  private String className;

  private String generatedClassName;

  public ReflectorGenerator(Class<?> hookInterface) {
    this.hookInterface = hookInterface;
    Reflected r = this.hookInterface.getAnnotation(Reflected.class); // if r is null, there's a
                                                                     // problem with their hook.
    this.className = r.className();
  }

  public ReflectorGenerator(Class<?> hookInterface, MapperManager mapManager) {
    this.hookInterface = hookInterface;
    this.mapManager = mapManager;
    Reflected r = this.hookInterface.getAnnotation(Reflected.class); // if r is null, there's a
                                                                     // problem with their hook.
    this.className = r.className();
  }

  private void initializeClass() {
    this.generatedClassName = this.hookInterface.getName().replace('.', '/') + "_Reflector";

    this.cip = new ClassWriter(ClassWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS);
    Reflected r = this.hookInterface.getAnnotation(Reflected.class);
    boolean obf = r != null && r.isObfuscated();
    this.cip.visit(Opcodes.V1_6, Opcodes.ACC_PUBLIC, this.generatedClassName, null,
        reflectorClassName, new String[] {this.hookInterface.getName().replace('.', '/')});
  }

  private void generateConstructor() {
    MethodVisitor mv =
        this.cip.visitMethod(Opcodes.ACC_PUBLIC, "<init>", "(Ljava/lang/Class;)V",
            "(Ljava/lang/Class<*>;)V", null);
    if (mv != null) {
      mv.visitCode();
      mv.visitVarInsn(Opcodes.ALOAD, 0);
      mv.visitVarInsn(Opcodes.ALOAD, 1);
      mv.visitMethodInsn(Opcodes.INVOKESPECIAL, reflectorClassName, "<init>",
          "(Ljava/lang/Class;)V", false);
      mv.visitInsn(Opcodes.RETURN);
      mv.visitMaxs(2, 2);
      mv.visitEnd();
    }
  }

  public void generateMethods() {
    boolean obf = this.hookInterface.getAnnotation(Reflected.class).isObfuscated();

    for (Method m : this.hookInterface.getDeclaredMethods()) {
      Annotation[] annotations = m.getAnnotations();
      if (annotations.length != 0) {
        for (Annotation a : annotations) {
          if (a instanceof Getter) {
            Getter b = (Getter) a;
            if (b.isObfuscated()) {
              System.out.println(this.className + '.' + b.fieldName());
              this.generateGetter(
                  m,
                  this.mapManager.fieldMappings.get(this.className.replace('/', '.') + '.'
                      + b.fieldName()).name);
            } else {
              this.generateGetter(m, b.fieldName());
            }
          } else if (a instanceof Setter) {
            Setter b = (Setter) a;
            if (b.isObfuscated()) {
              this.generateSetter(
                  m,
                  this.mapManager.fieldMappings.get(this.className.replace('/', '.') + '.'
                      + b.fieldName()).name);
            } else {
              this.generateSetter(m, b.fieldName());
            }
          } else if (a instanceof ExistingMethod) {
            this.generateMethodCaller(m);
          } else if (a instanceof CustomMethod) {
            this.generateCustomMethodCaller(m);
          } else if (a instanceof Constructor) {
            this.generateConstructor(m);
          }
        }
      }
    }
  }

  private void loadInt(MethodVisitor mv, int i) {
    switch (i) {
      case 0:
        mv.visitInsn(Opcodes.ICONST_0);
        break;
      case 1:
        mv.visitInsn(Opcodes.ICONST_1);
        break;
      case 2:
        mv.visitInsn(Opcodes.ICONST_2);
        break;
      case 3:
        mv.visitInsn(Opcodes.ICONST_3);
        break;
      case 4:
        mv.visitInsn(Opcodes.ICONST_4);
        break;
      case 5:
        mv.visitInsn(Opcodes.ICONST_5);
        break;
      default:
        if (i <= Byte.MAX_VALUE) {
          mv.visitIntInsn(Opcodes.BIPUSH, i);
        } else if (i <= Short.MAX_VALUE) {
          mv.visitIntInsn(Opcodes.SIPUSH, i);
        } else {
          mv.visitLdcInsn(i); // r u srs
        }
    }
  }


  private void generateCustomMethodCaller(Method method) {
    String customMethod = CustomMethod.class.getName().replace('.', '/');
    String desc = BytecodeUtils.getMethodSignature(method);

    MethodVisitor mv = this.cip.visitMethod(Opcodes.ACC_PUBLIC, method.getName(), desc, null, null);
    mv.visitCode();
    Label l0 = new Label();
    Label l1 = new Label();
    Label l2 = new Label();
    mv.visitTryCatchBlock(l0, l1, l2, "java/lang/Exception");
    Label l3 = new Label();
    Label l4 = new Label();
    mv.visitTryCatchBlock(l3, l4, l2, "java/lang/Exception");
    mv.visitLabel(l0);
    mv.visitVarInsn(Opcodes.ALOAD, 0);
    mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Object", "getClass",
        "()Ljava/lang/Class;", false);
    mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Class", "getDeclaredMethods",
        "()[Ljava/lang/reflect/Method;", false);
    mv.visitInsn(Opcodes.DUP);
    mv.visitVarInsn(Opcodes.ASTORE, 4);
    mv.visitInsn(Opcodes.ARRAYLENGTH);
    mv.visitVarInsn(Opcodes.ISTORE, 3);
    mv.visitInsn(Opcodes.ICONST_0);
    mv.visitVarInsn(Opcodes.ISTORE, 2);
    Label l5 = new Label();
    mv.visitJumpInsn(Opcodes.GOTO, l5);
    Label l6 = new Label();
    mv.visitLabel(l6);
    mv.visitFrame(Opcodes.F_FULL, 5, new Object[] {this.generatedClassName, Opcodes.TOP,
        Opcodes.INTEGER, Opcodes.INTEGER, "[Ljava/lang/reflect/Method;"}, 0, new Object[] {});
    mv.visitVarInsn(Opcodes.ALOAD, 4);
    mv.visitVarInsn(Opcodes.ILOAD, 2);
    mv.visitInsn(Opcodes.AALOAD);
    mv.visitVarInsn(Opcodes.ASTORE, 1);
    mv.visitVarInsn(Opcodes.ALOAD, 1);
    mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/reflect/Method", "getName",
        "()Ljava/lang/String;", false);
    mv.visitLdcInsn(method.getName());
    mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/String", "equals",
        "(Ljava/lang/Object;)Z", false);
    mv.visitJumpInsn(Opcodes.IFEQ, l3);


    mv.visitVarInsn(Opcodes.ALOAD, 1);
    mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/reflect/Method", "getParameterTypes",
        "()[Ljava/lang/Class;", false);
    mv.visitVarInsn(Opcodes.ASTORE, 5);

    mv.visitVarInsn(Opcodes.ALOAD, 5);
    mv.visitInsn(Opcodes.ARRAYLENGTH);

    Class<?>[] params = method.getParameterTypes();

    this.loadInt(mv, params.length);
    mv.visitJumpInsn(Opcodes.IF_ICMPEQ, l3);

    for (int i = 0; i < params.length; i++) {
      mv.visitVarInsn(Opcodes.ALOAD, 5);
      this.loadInt(mv, i);
      mv.visitInsn(Opcodes.AALOAD);
      mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Class", "getName",
          "()Ljava/lang/String;", false);
      mv.visitLdcInsn(params[i].getName());
      mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/String", "equals",
          "(Ljava/lang/Object;)Z", false);
      mv.visitJumpInsn(Opcodes.IFEQ, l3);
    }

    mv.visitVarInsn(Opcodes.ALOAD, 1);
    mv.visitLdcInsn(Type.getType('L' + customMethod + ';'));
    mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/reflect/Method", "getAnnotation",
        "(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;", false);
    mv.visitTypeInsn(Opcodes.CHECKCAST, customMethod);
    mv.visitMethodInsn(Opcodes.INVOKEINTERFACE, customMethod, "codeDefinition",
        "()Ljava/lang/Class;", true);
    mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Class", "newInstance",
        "()Ljava/lang/Object;", false);
    mv.visitTypeInsn(Opcodes.CHECKCAST, "java/lang/Runnable");
    mv.visitMethodInsn(Opcodes.INVOKEINTERFACE, "java/lang/Runnable", "run", "()V", true);
    mv.visitLabel(l1);
    mv.visitInsn(Opcodes.RETURN);
    mv.visitLabel(l3);
    mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
    mv.visitIincInsn(2, 1);
    mv.visitLabel(l5);
    mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
    mv.visitVarInsn(Opcodes.ILOAD, 2);
    mv.visitVarInsn(Opcodes.ILOAD, 3);
    mv.visitJumpInsn(Opcodes.IF_ICMPLT, l6);
    mv.visitLabel(l4);
    Label l10 = new Label();
    mv.visitJumpInsn(Opcodes.GOTO, l10);
    mv.visitLabel(l2);
    mv.visitFrame(Opcodes.F_FULL, 1, new Object[] {this.generatedClassName}, 1,
        new Object[] {"java/lang/Exception"});
    mv.visitVarInsn(Opcodes.ASTORE, 1);
    mv.visitVarInsn(Opcodes.ALOAD, 1);
    mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Exception", "printStackTrace", "()V",
        false);
    mv.visitLabel(l10);
    mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
    mv.visitInsn(Opcodes.RETURN);
    mv.visitMaxs(2, 6);
    mv.visitEnd();
  }

  private void generateMethodCaller(Method methodCaller) {
    ExistingMethod a = methodCaller.getAnnotation(ExistingMethod.class);
    Class<?>[] params = methodCaller.getParameterTypes();
    boolean isStatic = params.length == 0 || a.isStatic();

    String desc = BytecodeUtils.getMethodSignature(methodCaller);
    Type ret = Type.getReturnType(methodCaller);
    MethodVisitor mv =
        this.cip.visitMethod(Opcodes.ACC_PUBLIC, methodCaller.getName(), desc, null, new String[0]);
    // TODO add support for generics.
    String callingName = a.name();
    String callingDescriptor = a.descriptor();

    Type[] paramTypes = Type.getArgumentTypes(methodCaller);

    if (a.isObfuscated()) {
      String n = (this.className.replace('/', '.') + '.' + callingName + callingDescriptor);
      MethodNode mn = this.mapManager.methodMappings.get(n);
      if (mn != null) {
        callingName = mn.name;
        callingDescriptor = mn.desc;
      } else {
        String availableMappingsList = "";
        for (Entry<String, MethodNode> e : this.mapManager.methodMappings.entrySet()) {
          availableMappingsList += e.getKey() + " -> " + e.getValue().name + "\r";
        }

        System.err.println("Requested method hook failed to find a name mapping! Details:\r"
            + String.format("Requested mapping: %s\r", n)
            + String.format("Mappings available: \r%s", availableMappingsList));
      }
    }

    int localVarIndex = 0;

    mv.visitCode();
    Label l0 = new Label();
    Label l1 = new Label();
    Label l2 = new Label();
    mv.visitTryCatchBlock(l0, l1, l2, "java/lang/Exception");
    mv.visitLabel(l0);
    mv.visitVarInsn(Opcodes.ALOAD, 0);
    if (isStatic) {
      mv.visitInsn(Opcodes.ACONST_NULL);
    } else {
      mv.visitVarInsn(Opcodes.ALOAD, 1);
      localVarIndex = 1;
    }

    mv.visitLdcInsn(callingName);
    int arrayLength = params.length - localVarIndex;
    this.loadInt(mv, arrayLength);
    mv.visitTypeInsn(Opcodes.ANEWARRAY, "java/lang/Object");

    for (int i = 0; i < arrayLength; i++) {
      mv.visitInsn(Opcodes.DUP);
      this.loadInt(mv, i);

      mv.visitVarInsn(paramTypes[i].getOpcode(Opcodes.ILOAD), i + localVarIndex + 1);
      switch (paramTypes[i].getSort()) {
        case Type.INT:
          mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Integer", "valueOf",
              "(I)Ljava/lang/Integer;", false);
          break;
        case Type.LONG:
          mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Long", "valueOf",
              "(J)Ljava/lang/Long;", false);
          break;
        case Type.BYTE:
          mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Byte", "valueOf",
              "(B)Ljava/lang/Byte;", false);
          break;
        case Type.SHORT:
          mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Short", "valueOf",
              "(S)Ljava/lang/Short;", false);
          break;
        case Type.FLOAT:
          mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Float", "valueOf",
              "(F)Ljava/lang/Float;", false);
          break;
        case Type.DOUBLE:
          mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Double", "valueOf",
              "(D)Ljava/lang/Double;", false);
          break;
        case Type.BOOLEAN:
          mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Boolean", "valueOf",
              "(Z)Ljava/lang/Boolean;", false);
          break;
        case Type.CHAR:
          mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Character", "valueOf",
              "(C)Ljava/lang/Character;", false);
      }
      mv.visitInsn(Opcodes.AASTORE);
    }


    mv.visitMethodInsn(Opcodes.INVOKESPECIAL, Reflector.class.getName().replace('.', '/'),
        "invoke", "(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;",
        false);
    if (ret.getSort() == Type.VOID) {
      mv.visitInsn(Opcodes.POP);
    }
    mv.visitLabel(l1);
    if (ret.getSort() != Type.VOID) {
      mv.visitInsn(ret.getOpcode(Opcodes.IRETURN));
    }
    Label l3 = new Label();
    if (ret.getSort() == Type.VOID) {
      mv.visitJumpInsn(Opcodes.GOTO, l3);
    }
    mv.visitLabel(l2);
    mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Exception"});
    mv.visitVarInsn(Opcodes.ASTORE, 1);
    mv.visitVarInsn(Opcodes.ALOAD, 1);
    mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Exception", "printStackTrace", "()V",
        false);
    mv.visitLabel(l3);
    mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
    this.generateSadReturnCase(mv, ret);
    mv.visitMaxs(4, 3);
    mv.visitEnd();
  }

  public Object test() {
    try {
      return getClass().getMethod("lol").invoke(null);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  private void generateGetter(Method getter, String fieldName) {
    String desc = BytecodeUtils.getMethodSignature(getter);
    Type ret = Type.getReturnType(getter);
    boolean isStatic = getter.getParameterTypes().length == 0;
    MethodVisitor mv =
        this.cip.visitMethod(Opcodes.ACC_PUBLIC, getter.getName(), desc, null, new String[0]);
    if (mv != null) {
      // TODO make this a lot better for everyone by NOT USING REFLECTION.
      // Though, that'd mean either only using reflection for non-public types or
      // changing all the fields to public. :\
      // whatever. we can change it after we get this shit working
      mv.visitCode();
      Label l0 = new Label();
      Label l1 = new Label();
      Label l2 = new Label();
      mv.visitTryCatchBlock(l0, l1, l2, "java/lang/Exception");
      mv.visitLabel(l0);
      mv.visitVarInsn(Opcodes.ALOAD, 0);
      mv.visitLdcInsn(fieldName);
      mv.visitMethodInsn(Opcodes.INVOKESPECIAL, Reflector.class.getName().replace('.', '/'),
          "getField", "(Ljava/lang/String;)Ljava/lang/reflect/Field;", false);

      if (isStatic) {
        mv.visitInsn(Opcodes.ACONST_NULL);
      } else {
        mv.visitVarInsn(Opcodes.ALOAD, 1);
      }

      mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/reflect/Field", "get",
          "(Ljava/lang/Object;)Ljava/lang/Object;", false);

      // |=-=-=-=-=-=-=-=-=-=-=|
      // | TYPE CASTING!!!! :D |
      // |=-=-=-=-=-=-=-=-=-=-=|
      //
      // #funcomments
      String returnTypeCast = "java/lang/";
      String primCastName = null;
      String primCastDesc = null;
      switch (ret.getSort()) {
        case Type.ARRAY:
        case Type.OBJECT:
          returnTypeCast = ret.getInternalName();
          break;
        case Type.INT:
          returnTypeCast += "Integer";
          primCastName = "intValue";
          primCastDesc = "()I";
          break;
        case Type.LONG:
          returnTypeCast += "Long";
          primCastName = "longValue";
          primCastDesc = "()J";
          break;
        case Type.FLOAT:
          returnTypeCast += "Float";
          primCastName = "floatValue";
          primCastDesc = "()F";
          break;
        case Type.DOUBLE:
          returnTypeCast += "Double";
          primCastName = "doubleValue";
          primCastDesc = "()D";
          break;
        case Type.SHORT:
          returnTypeCast += "Short";
          primCastName = "shortValue";
          primCastDesc = "()S";
          break;
        case Type.CHAR:
          returnTypeCast += "Character";
          primCastName = "charValue";
          primCastDesc = "()C";
          break;
        case Type.BYTE:
          returnTypeCast += "Byte";
          primCastName = "byteValue";
          primCastDesc = "()B";
          break;
        case Type.BOOLEAN:
          returnTypeCast += "Boolean";
          primCastName = "booleanValue";
          primCastDesc = "()Z";
      }
      if ((ret.getSort() != Type.OBJECT && ret.getSort() != Type.ARRAY)
          || !"java/lang/Object".equals(ret.getClassName())) {
        mv.visitTypeInsn(Opcodes.CHECKCAST, returnTypeCast);
      }
      if (primCastName != null) {
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, returnTypeCast, primCastName, primCastDesc, false);
      }

      mv.visitLabel(l1);
      mv.visitInsn(ret.getOpcode(Opcodes.IRETURN));
      mv.visitLabel(l2);
      mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Exception"});
      mv.visitVarInsn(Opcodes.ASTORE, 2);
      mv.visitVarInsn(Opcodes.ALOAD, 2);
      mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Exception", "printStackTrace", "()V",
          false);

      this.generateSadReturnCase(mv, ret);

      mv.visitMaxs(2, 3);
      mv.visitEnd();
    }
  }

  private void generateSetter(Method setter, String fieldName) {
    String desc = BytecodeUtils.getMethodSignature(setter);
    Type ret = Type.getArgumentTypes(setter)[1];
    boolean isStatic = setter.getParameterTypes().length == 1;
    MethodVisitor mv =
        this.cip.visitMethod(Opcodes.ACC_PUBLIC, setter.getName(), desc, null, new String[0]);

    if (mv != null) {
      mv.visitCode();
      Label l0 = new Label();
      Label l1 = new Label();
      Label l2 = new Label();
      mv.visitTryCatchBlock(l0, l1, l2, "java/lang/Exception");
      mv.visitLabel(l0);
      mv.visitVarInsn(Opcodes.ALOAD, 0);
      mv.visitLdcInsn(fieldName);
      mv.visitMethodInsn(Opcodes.INVOKESPECIAL, Reflector.class.getName().replace('.', '/'),
          "getField", "(Ljava/lang/String;)Ljava/lang/reflect/Field;", false);
      mv.visitVarInsn(Opcodes.ALOAD, 1);
      mv.visitVarInsn(ret.getOpcode(Opcodes.ILOAD), 2);

      String type = "java/lang/";
      switch (ret.getSort()) {
        case Type.ARRAY:
        case Type.OBJECT:
          type = null;
          break;
        case Type.INT:
          type += "Integer";
          break;
        case Type.LONG:
          type += "Long";
          break;
        case Type.FLOAT:
          type += "Float";
          break;
        case Type.DOUBLE:
          type += "Double";
          break;
        case Type.SHORT:
          type += "Short";
          break;
        case Type.CHAR:
          type += "Character";
          break;
        case Type.BYTE:
          type += "Byte";
          break;
        case Type.BOOLEAN:
          type += "Boolean";
      }
      if (type != null) {
        mv.visitMethodInsn(Opcodes.INVOKESTATIC, type, "valueOf", '(' + ret.getDescriptor() + ")L"
            + type + ';', false);
      }
      mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/reflect/Field", "set",
          "(Ljava/lang/Object;Ljava/lang/Object;)V", false);
      mv.visitLabel(l1);
      Label l3 = new Label();
      mv.visitJumpInsn(Opcodes.GOTO, l3);
      mv.visitLabel(l2);
      mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Exception"});
      mv.visitVarInsn(Opcodes.ASTORE, 3);
      mv.visitVarInsn(Opcodes.ALOAD, 3);
      mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Exception", "printStackTrace", "()V",
          false);
      mv.visitLabel(l3);
      mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
      mv.visitInsn(Opcodes.RETURN);
      mv.visitMaxs(3, 4);
      mv.visitEnd();
    }
  }

  private void generateConstructor(Method method) {
    Class<?>[] paramTypes = method.getParameterTypes();
    MethodVisitor mv =
        this.cip.visitMethod(method.getModifiers() & ~Opcodes.ACC_ABSTRACT, method.getName(),
            Type.getMethodDescriptor(method), null, null);
    if (mv != null) {
      mv.visitCode();

      Label l0 = new Label();
      Label l1 = new Label();
      Label l2 = new Label();
      mv.visitTryCatchBlock(l0, l1, l2, "java/lang/Exception");
      mv.visitLabel(l0);
      mv.visitVarInsn(Opcodes.ALOAD, 0);
      this.loadInt(mv, paramTypes.length);
      mv.visitTypeInsn(Opcodes.ANEWARRAY, "java/lang/Object");
      mv.visitVarInsn(Opcodes.ASTORE, paramTypes.length + 1);
      for (int i = 0; i < paramTypes.length; i++) {
        mv.visitVarInsn(Opcodes.ALOAD, paramTypes.length + 1);
        this.loadInt(mv, i);
        Type t = Type.getType(paramTypes[i]);
        mv.visitVarInsn(t.getOpcode(Opcodes.ILOAD), i + 1);

        switch (t.getSort()) {
          case Type.ARRAY:
          case Type.OBJECT:
            break;
          case Type.INT:
            mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Integer", "valueOf",
                "(I)Ljava/lang/Integer;", false);
            break;
          case Type.LONG:
            mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Long", "valueOf",
                "(J)Ljava/lang/Long;", false);
            break;
          case Type.CHAR:
            mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Character", "valueOf",
                "(C)Ljava/lang/Character;", false);
            break;
          case Type.BYTE:
            mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Byte", "valueOf",
                "(B)Ljava/lang/Byte;", false);
            break;
          case Type.SHORT:
            mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Short", "valueOf",
                "(S)Ljava/lang/Short;", false);
            break;
          case Type.FLOAT:
            mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Float", "valueOf",
                "(F)Ljava/lang/Float;", false);
            break;
          case Type.DOUBLE:
            mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Double", "valueOf",
                "(D)Ljava/lang/Double;", false);
            break;
          case Type.BOOLEAN:
            mv.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Boolean", "valueOf",
                "(Z)Ljava/lang/Boolean;", false);
        }
        mv.visitInsn(Opcodes.AASTORE);
      }
      mv.visitVarInsn(Opcodes.ALOAD, paramTypes.length + 1);
      mv.visitMethodInsn(Opcodes.INVOKESPECIAL, Reflector.class.getName().replace('.', '/'),
          "instantiate", "([Ljava/lang/Object;)Ljava/lang/Object;", false);
      mv.visitLabel(new Label());
      mv.visitInsn(Opcodes.ARETURN);
      mv.visitLabel(l1);
      mv.visitLabel(l2);
      mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] {"java/lang/Exception"});
      mv.visitVarInsn(Opcodes.ASTORE, paramTypes.length + 1);
      mv.visitVarInsn(Opcodes.ALOAD, paramTypes.length + 1);
      mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/lang/Exception", "printStackTrace", "()V",
          false);
      mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
      mv.visitInsn(Opcodes.ACONST_NULL);
      mv.visitInsn(Opcodes.ARETURN);

      mv.visitMaxs(paramTypes.length + 3, paramTypes.length + 3);
      mv.visitEnd();
    }
  }

  private void endClass() {
    this.cip.visitEnd();
  }

  public Class<? extends Reflector> generate() {
    this.initializeClass();
    this.generateConstructor();
    this.generateMethods();
    this.endClass();

    byte[] code = this.cip.toByteArray();


    ClassReader cr = new ClassReader(code);

    CheckClassAdapter cca =
        new CheckClassAdapter(new ClassWriter(cr, ClassWriter.COMPUTE_MAXS
            | ClassWriter.COMPUTE_FRAMES), true);
    cr.accept(cca, 0);
    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);
    CheckClassAdapter.verify(new ClassReader(code), true, pw);

    // // sw.flush();
    // // try {
    // // sw.close();
    // // } catch (IOException e) {
    // // e.printStackTrace();
    // // }
    // // pw.close();
    // sw = new StringWriter();
    // pw = new PrintWriter(sw);
    // TraceClassVisitor tcv = new TraceClassVisitor(pw);
    // cr.accept(tcv, 0);
    // for (Object o : tcv.p.text) {
    // System.out.println(o);
    // }


    // debug
    /*
     * try (FileOutputStream fos = new FileOutputStream(new File("./TestReflector.class"))) {
     * fos.write(code); fos.flush(); } catch (Exception e) { e.printStackTrace(); }
     */
    // debug



    return (Class<? extends Reflector>) ReflectorLoader.defineReflector(hookInterface.getName()
        + "_Reflector", code, 0, code.length);
  }

  private void generateSadReturnCase(MethodVisitor mv, Type returnType) {
    switch (returnType.getSort()) {
      case Type.ARRAY:
      case Type.OBJECT:
        mv.visitInsn(Opcodes.ACONST_NULL);
        mv.visitInsn(Opcodes.ARETURN);
        break;
      case Type.BOOLEAN:
      case Type.BYTE:
      case Type.SHORT:
      case Type.INT:
        mv.visitInsn(Opcodes.ICONST_0);
        mv.visitInsn(Opcodes.IRETURN);
        break;
      case Type.FLOAT:
        mv.visitInsn(Opcodes.FCONST_0);
        mv.visitInsn(Opcodes.FRETURN);
        break;
      case Type.DOUBLE:
        mv.visitInsn(Opcodes.DCONST_0);
        mv.visitInsn(Opcodes.DRETURN);
        break;
      case Type.LONG:
        mv.visitInsn(Opcodes.LCONST_0);
        mv.visitInsn(Opcodes.LRETURN);
        break;
      case Type.VOID:
        mv.visitInsn(Opcodes.RETURN);
    }
  }

  public static final class ReflectorLoader extends ClassLoader {
    private static ReflectorLoader ins = new ReflectorLoader();

    public static Class<?> defineReflector(String name, byte[] bytecode, int off, int len) {
      return ins.defineClass(name, bytecode, off, len);
    }
  }
}
