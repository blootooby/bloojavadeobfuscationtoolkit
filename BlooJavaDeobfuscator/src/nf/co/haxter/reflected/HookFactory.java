package nf.co.haxter.reflected;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nf.co.haxter.mapper.MapperManager;
import nf.co.haxter.reflected.interfacer.Hook;

import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

/**
 * @author blootooby
 * @since July 29, 2014
 */
public final class HookFactory {
  // FACTORY ACCESS CRAP
  private static HookFactory dfltInstance = new HookFactory();

  private HookFactory() {}

  public static HookFactory getDefault() {
    return dfltInstance;
  }

  // STANDARD SHIT ^, MOVE ALONG PEOPLE

  private List<Reflector> reflectors = new ArrayList<Reflector>();

  public void addReflector(Reflector reflector) {
    if (!isReflectorValid(reflector) || this.reflectors.contains(reflector)) {
      return;
    }
    this.reflectors.add(reflector);
  }

  private boolean isReflectorValid(Reflector reflector) {
    for (Class<?> i : reflector.getClass().getInterfaces()) {
      if (Hook.class.isAssignableFrom(i)) {
        return true;
      }
    }
    return false;
  }

  /*
   * public <T extends Reflector> T getReflector(Class<T> clazz) { for (Reflector r :
   * this.reflectors) { if (clazz.equals(r.getClass())) { return (T) r; } } return null; }
   */

  public <T extends Hook> T getHook(Class<T> clazz) {
    for (Reflector r : this.reflectors) {
      if (clazz.isAssignableFrom(r.getClass())) {
        return (T) r;
      }
    }
    return null;
  }

  public void createHook(Class<?> classToHook, Class<? extends Hook> hook) {
    try {
      this.addReflector(new ReflectorGenerator(hook).generate().getConstructor(Class.class)
          .newInstance(classToHook));
    } catch (InstantiationException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    } catch (IllegalArgumentException e) {
      e.printStackTrace();
    } catch (InvocationTargetException e) {
      e.printStackTrace();
    } catch (NoSuchMethodException e) {
      e.printStackTrace();
    } catch (SecurityException e) {
      e.printStackTrace();
    }
  }

  public void createHook(Class<?> hooking, Class<? extends Hook> hook, MapperManager mapperManager) {
    try {
      this.addReflector(new ReflectorGenerator(hook, mapperManager).generate()
          .getConstructor(Class.class).newInstance(hooking));
      for (Field f : hooking.getDeclaredFields()) {
        f.setAccessible(true);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
