package nf.co.haxter.mapper;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;

import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Handle;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.TypePath;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.MethodNode;

/**
 * A utility class for class-mapping.
 * 
 * @author blootooby
 * @since July 14, 2014.
 */
public final class MappingUtils {

  private MappingUtils() {}

  /**
   * Checks if the method contains the provided instruction type.
   * 
   * @param method the method to check.
   * @param opcode the instruction type opcode.
   * @return if the method contains an instruction that corresponds to the provided opcode.
   * @see AbstractInsnNode
   * @see AbstractInsnNode#getOpcode()
   */
  public static boolean containsInstructionType(MethodNode method, int opcode) {
    return containsInstructionType(method.instructions, opcode);
  }

  /**
   * Checks if the {@link InsnList} contains the provided instruction type.
   * 
   * @param method the method to check.
   * @param opcode the instruction type opcode.
   * @return if the method contains an instruction that corresponds to the provided opcode.
   * @see AbstractInsnNode
   * @see AbstractInsnNode#getOpcode()
   */
  public static boolean containsInstructionType(InsnList insns, int opcode) {
    ListIterator<AbstractInsnNode> l = insns.iterator();
    while (l.hasNext()) {
      if (l.next().getOpcode() == opcode) {
        return true;
      }
    }
    return false;
  }

  /**
   * Checks if the {@link List} of {@link AbstractInsnNode}s contains the provided instruction type.
   * 
   * @param method the method to check.
   * @param opcode the instruction type opcode.
   * @return if the method contains an instruction that corresponds to the provided opcode.
   * @see AbstractInsnNode
   * @see AbstractInsnNode#getOpcode()
   */
  public static boolean containsInstructionType(List<AbstractInsnNode> insns, int opcode) {
    ListIterator<AbstractInsnNode> l = insns.listIterator();
    while (l.hasNext()) {
      if (l.next().getOpcode() == opcode) {
        return true;
      }
    }
    return false;
  }

  /**
   * Another way of retrieving {@link ClassNode}s from an instance of {@link MapperManager}.
   * 
   * @param mapperManager the {@link MapperManager} to retrieve the {@link ClassNode} from.
   * @param className the name of the class to be retrieved.
   * @return a {@link ClassNode} from the passed in {@link MapperManager}.
   */
  public static ClassNode getClassNodeFromMapperManager(MapperManager mapperManager,
      String className) {
    return mapperManager.classMappings.get(className);
  }

  /**
   * Gets a {@link ClassNode} from a {@link JarFile} based on the class name.
   * 
   * @param jar the {@link JarFile} containing the class.
   * @param className the name of the class contained by the {@link JarFile}.
   * @return the {@link ClassNode} loaded from the {@link JarFile}.
   * @throws IOException if something goes wrong with loading from the {@link JarFile}.
   */
  public static ClassNode getClassNodeFromJarEntry(JarFile jar, String className)
      throws IOException {
    ClassNode cn = new ClassNode(Opcodes.ASM4);
    getClassReaderFromJar(jar, className).accept(cn, 0);
    return cn;
  }

  /**
   * A convenience method to get a {@link List} of {@link MethodNode}s without having to add a
   * {@link SuppressWarnings} annotation to all of your methods.
   * 
   * @param cn the {@link ClassNode} that has this {@link List} of {@link MethodNode}s.
   * @return a {@link List} of {@link MethodNode}s.
   * @see org.objectweb.asm.tree.ClassNode#methods
   */
  @SuppressWarnings("unchecked")
  public static List<MethodNode> getMethods(ClassNode cn) {
    return cn.methods;
  }

  /**
   * A convenience method to get a {@link List} of {@link FieldNode}s without having to add a
   * {@link SuppressWarnings} annotation to all of your methods.
   * 
   * @param cn the {@link ClassNode} that has this {@link List} of {@link FieldNode}s.
   * @return a {@link List} of {@link FieldNode}s.
   * @see org.objectweb.asm.tree.ClassNode#fields
   */
  @SuppressWarnings("unchecked")
  public static List<FieldNode> getFields(ClassNode cn) {
    return cn.fields;
  }

  /**
   * A convenience method to get the {@link InputStream} of a {@link JarEntry} from a
   * {@link JarFile}.
   * 
   * @param jar the {@link JarFile} containing the {@link JarEntry}.
   * @param entry the name of the {@link JarEntry} in the jar file.
   * @return the {@link InputStream} of the {@link JarEntry}.
   * @throws IOException if an I/O error has occured from attempting to get the {@link InputStream}.
   */
  public static InputStream getInputStreamFromJar(JarFile jar, String entry) throws IOException {
    ZipEntry e = jar.getEntry(entry);
    if (e == null) {
      System.err.println("Failed to get JarEntry \"" + entry + "\"!");
      new Exception().printStackTrace();
    }
    return jar.getInputStream(e);
  }

  /**
   * A convenience method that gets a {@link ClassReader} to analyze a class file stored in a
   * {@link JarFile}.
   * 
   * @param jar the {@link JarFile} containing the class.
   * @param className the fully qualified class name of the class stored in the {@link JarFile}.
   * @return a {@link ClassReader} with the input set to the class stored in the {@link JarFile}.
   * @throws IOException if an I/O error occurs from attempting to get the {@link InputStream} of
   *         the class file.
   * @see MappingUtils#getInputStreamFromJar(JarFile, String)
   * @see org.objectweb.asm.ClassReader#ClassReader(InputStream)
   * @see java.io.InputStream
   * @see java.util.jar.JarFile
   */
  public static ClassReader getClassReaderFromJar(JarFile jar, String className) throws IOException {
    className = className.replace('.', '/');
    if (!className.endsWith("/class")) {
      className = className + ".class";
    } else {
      className = className.substring(0, className.length() - 6) + ".class";
    }
    return new ClassReader(getInputStreamFromJar(jar, className));
  }

  /**
   * A convenience method for getting the bytecode of a {@link Class} from a {@link ClassReader}.
   * 
   * @param cr the {@link ClassReader} that is analyzing the class.
   * @param flags the flags to use for the {@link ClassWriter}.
   * @return the bytecode of the class.
   */
  public static byte[] getBytecode(ClassReader cr, int flags) {
    return new ClassWriter(cr, flags).toByteArray();
  }

  /**
   * Gets an array of parameter types from a method signature.
   * 
   * @param map a {@link Map} of class mappings.
   * @param cl a {@link ClassLoader} to load the parameter types.
   * @param methodSignature the method signature to parse.
   * @return an array of parameter types parsed from the method signature.
   * @throws ClassNotFoundException if a parameter type does not exist.
   * @see Class
   * @see ClassLoader
   * @see org.objectweb.asm.tree.MethodNode#desc
   */
  public static Class<?>[] parseParameterTypesFromMethodSignature(Map<String, ClassNode> map,
      ClassLoader cl, String methodSignature) throws ClassNotFoundException {
    StringIter iter = new StringIter(methodSignature);
    ArrayList<Class<?>> claz = new ArrayList<Class<?>>();
    while (iter.peek() != ')') {
      claz.add(parseNextType(map, cl, iter));
    }
    Class<?>[] p = new Class<?>[claz.size()];
    for (int i = 0; i < p.length; i++) {
      p[i] = claz.get(i);
    }
    return p;
  }

  /**
   * Gets the return type from a method signature.
   * 
   * @param map a {@link Map} of class mappings.
   * @param cl a {@link ClassLoader} for loading the class of the return type.
   * @param methodSignature the method signature to parse.
   * @return The return type parsed from the method signature.
   * @throws ClassNotFoundException if the ClassLoader fails to load the return type.
   */
  public static Class<?> parseReturnTypeFromMethodSignature(Map<String, ClassNode> map,
      ClassLoader cl, String methodSignature) throws ClassNotFoundException {
    StringIter iter = new StringIter(methodSignature, methodSignature.indexOf(')'));
    return parseNextType(map, cl, iter);
  }

  /**
   * Parses a type from a {@link StringIter}.
   * 
   * @param map a {@link Map} of class mappings.
   * @param cl a {@link ClassLoader} to load the type.
   * @param iterator a {@link StringIter} with types.
   * @return the type that was parsed from the {@link StringIter}.
   * @throws ClassNotFoundException if the ClassLoader fails to load the type.
   */
  private static Class<?> parseNextType(Map<String, ClassNode> map, ClassLoader cl,
      StringIter iterator) throws ClassNotFoundException {
    char c = iterator.nextChar();
    switch (c) {
      case 'I':
        return Integer.TYPE;
      case 'Z':
        return Boolean.TYPE;
      case 'V':
        return Void.TYPE;
      case 'J':
        return Long.TYPE;
      case 'B':
        return Byte.TYPE;
      case 'D':
        return Double.TYPE;
      case 'F':
        return Float.TYPE;
      case 'C':
        return Character.TYPE;
      case 'S':
        return Short.TYPE;
      case 'L':
        String s = iterator.readUntilAndIncrement(';').replace('/', '.');
        s = s.substring(0, s.length() - 1);
        return loadObf(map, cl, s);
      case '[':
        iterator.incrementIndex();
        return Array.newInstance(loadObf(map, cl, iterator.readUntilAndIncrement(';')), 0)
            .getClass();
      default:
        return null;
    }
  }

  /**
   * Loads a class when it's unknown if it's an obfuscated class or not.
   * 
   * @param map a {@link Map} of class mappings.
   * @param cl a {@link ClassLoader} to load the class.
   * @param s the class name.
   * @return the {@link Class} loaded from the ClassLoader
   * @throws ClassNotFoundException if the ClassLoader cannot find a mapping for the class after
   *         using the class map.
   */
  private static Class<?> loadObf(Map<String, ClassNode> map, ClassLoader cl, String s)
      throws ClassNotFoundException {
    String s1 = map.get(s.replace('/', '.')).name;
    if (s1 == null) {
      return cl.loadClass(s);
    } else {
      return cl.loadClass(s1);
    }
  }

  /**
   * Gets a {@link ArrayList} of classes that were referenced by the passed in class. (Represented
   * by a {@link ClassReader}).
   * 
   * @param cr a {@link ClassReader} to read the {@link Class} from.
   * @return an {@link ArrayList} of classes that were referenced in the passed in class.
   */
  public static Collection<String> getReferencedClasses(ClassReader cr) {
    final Collection<String> ret = new NonDupList<String>();
    ClassVisitor cv = new ClassVisitor(Opcodes.ASM4) {

      @Override
      public void visitOuterClass(String owner, String name, String desc) {
        super.visitOuterClass(owner, name, desc);
      }

      @Override
      public void visitInnerClass(String name, String outerName, String innerName, int access) {
        ret.add(name.replace('/', '.'));
        if (outerName != null) {
          ret.add(outerName.replace('/', '.'));
        }
        super.visitInnerClass(name, outerName, innerName, access);
      }

      @Override
      public void visit(int version, int access, String name, String signature, String superName,
          String[] interfaces) {
        ret.add(name.replace('/', '.'));
        ret.add(superName.replace('/', '.'));
        super.visit(version, access, name, signature, superName, interfaces);
      }

      @Override
      public FieldVisitor visitField(int access, String name, String desc, String signature,
          Object value) {
        if (desc.charAt(0) == 'L') {
          ret.add(desc.substring(1, desc.length() - 1).replace('/', '.'));
        }
        return super.visitField(access, name, desc, signature, value);
      }

      @Override
      public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
        for (String s : pickOutTypes(desc)) {
          ret.add(s);
        }
        return super.visitAnnotation(desc, visible);
      }

      @Override
      public AnnotationVisitor visitTypeAnnotation(int typeRef, TypePath typePath, String desc,
          boolean visible) {
        for (String s : pickOutTypes(desc)) {
          ret.add(s);
        }
        return super.visitTypeAnnotation(typeRef, typePath, desc, visible);
      }

      @Override
      public MethodVisitor visitMethod(int access, String name, String desc, String signature,
          String[] exceptions) {
        if (exceptions != null && exceptions.length != 0) {
          for (String s : exceptions) {
            ret.add(s.replace('/', '.'));
          }
        }

        MethodVisitor mv =
            new MethodVisitor(Opcodes.ASM4, super.visitMethod(access, name, desc, signature,
                exceptions)) {

              @Override
              public void visitTypeInsn(int opcode, String type) {
                ret.add(type.replace('/', '.'));
                super.visitTypeInsn(opcode, type);
              }

              @Override
              public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
                for (String s : pickOutTypes(desc)) {
                  ret.add(s);
                }
                return super.visitAnnotation(desc, visible);
              }

              @Override
              public AnnotationVisitor visitTypeAnnotation(int typeRef, TypePath typePath,
                  String desc, boolean visible) {
                return super.visitTypeAnnotation(typeRef, typePath, desc, visible);
              }

              @Override
              public void visitMethodInsn(int opcode, String owner, String name, String desc,
                  boolean itf) {
                for (String s : pickOutTypes(desc)) {
                  ret.add(s);
                }
                if (owner != null) {
                  ret.add(owner.replace('/', '.'));
                }
                super.visitMethodInsn(opcode, owner, name, desc, itf);
              }

              @Override
              public void visitInvokeDynamicInsn(String name, String desc, Handle bsm,
                  Object... bsmArgs) {
                for (String s : pickOutTypes(desc)) {
                  ret.add(s);
                }
                super.visitInvokeDynamicInsn(name, desc, bsm, bsmArgs);
              }

              @Override
              public void visitFieldInsn(int opcode, String owner, String name, String desc) {
                if (owner != null) {
                  ret.add(owner.replace('/', '.'));
                }
                for (String s : pickOutTypes(desc)) {
                  ret.add(s);
                }
                super.visitFieldInsn(opcode, owner, name, desc);
              }

            };


        return mv;
      }

    };

    cr.accept(cv, 0);

    return ret;
  }

  /**
   * Gets a {@link ArrayList} of classes that were referenced by the passed in class. (Represented
   * by a {@link ClassNode}).
   * 
   * @param cr a {@link ClassNode} to read.
   * @return an {@link ArrayList} of classes that were referenced in the passed in class.
   */
  public static Collection<String> getReferencedClasses(ClassNode cn) {
    final Collection<String> ret = new NonDupList<String>();
    ClassVisitor cv = new ClassVisitor(Opcodes.ASM4) {
      @Override
      public void visitOuterClass(String owner, String name, String desc) {
        super.visitOuterClass(owner, name, desc);
      }

      @Override
      public void visitInnerClass(String name, String outerName, String innerName, int access) {
        ret.add(name.replace('/', '.'));
        if (outerName != null) {
          ret.add(outerName.replace('/', '.'));
        }
        super.visitInnerClass(name, outerName, innerName, access);
      }

      @Override
      public void visit(int version, int access, String name, String signature, String superName,
          String[] interfaces) {
        ret.add(name.replace('/', '.'));
        ret.add(superName.replace('/', '.'));
        super.visit(version, access, name, signature, superName, interfaces);
      }

      @Override
      public FieldVisitor visitField(int access, String name, String desc, String signature,
          Object value) {
        if (desc.charAt(0) == 'L') {
          ret.add(desc.substring(1, desc.length() - 1).replace('/', '.'));
        }
        return super.visitField(access, name, desc, signature, value);
      }

      @Override
      public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
        for (String s : pickOutTypes(desc)) {
          ret.add(s);
        }
        return super.visitAnnotation(desc, visible);
      }

      @Override
      public AnnotationVisitor visitTypeAnnotation(int typeRef, TypePath typePath, String desc,
          boolean visible) {
        for (String s : pickOutTypes(desc)) {
          ret.add(s);
        }
        return super.visitTypeAnnotation(typeRef, typePath, desc, visible);
      }

      @Override
      public MethodVisitor visitMethod(int access, String name, String desc, String signature,
          String[] exceptions) {
        if (exceptions != null && exceptions.length != 0) {
          for (String s : exceptions) {
            ret.add(s.replace('/', '.'));
          }
        }
        MethodVisitor mv =
            new MethodVisitor(Opcodes.ASM4, super.visitMethod(access, name, desc, signature,
                exceptions)) {

              @Override
              public void visitTypeInsn(int opcode, String type) {
                ret.add(type.replace('/', '.'));
                super.visitTypeInsn(opcode, type);
              }

              @Override
              public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
                for (String s : pickOutTypes(desc)) {
                  ret.add(s);
                }
                return super.visitAnnotation(desc, visible);
              }

              @Override
              public AnnotationVisitor visitTypeAnnotation(int typeRef, TypePath typePath,
                  String desc, boolean visible) {
                return super.visitTypeAnnotation(typeRef, typePath, desc, visible);
              }

              @Override
              public void visitMethodInsn(int opcode, String owner, String name, String desc,
                  boolean itf) {
                for (String s : pickOutTypes(desc)) {
                  ret.add(s);
                }
                if (owner != null) {
                  ret.add(owner.replace('/', '.'));
                }
                super.visitMethodInsn(opcode, owner, name, desc, itf);
              }

              @Override
              public void visitInvokeDynamicInsn(String name, String desc, Handle bsm,
                  Object... bsmArgs) {
                for (String s : pickOutTypes(desc)) {
                  ret.add(s);
                }
                super.visitInvokeDynamicInsn(name, desc, bsm, bsmArgs);
              }

              @Override
              public void visitFieldInsn(int opcode, String owner, String name, String desc) {
                if (owner != null) {
                  ret.add(owner.replace('/', '.'));
                }
                for (String s : pickOutTypes(desc)) {
                  ret.add(s);
                }
                super.visitFieldInsn(opcode, owner, name, desc);
              }
            };
        return mv;
      }
    };
    cn.accept(cv);
    return ret;
  }

  /**
   * TODO
   * 
   * @param mn
   * @param ldcValue
   * @return
   */
  @Deprecated
  public static int getIndexLdcValueUsed(MethodNode mn, final Object ldcValue) {
    if (ldcValue == null) {
      return -1;
    }

    final class IntHolder {
      int val = -1;
    }
    InsnList insns = mn.instructions;
    final IntHolder index = new IntHolder();
    insns.accept(new MethodVisitor(Opcodes.ASM4) {
      @Override
      public void visitLdcInsn(Object cst) {
        if (ldcValue.equals(cst)) {
          // TODO FIXME
          index.val = 1;
        }
        super.visitLdcInsn(cst);
      }
    });

    return index.val;
  }

  /**
   * Checks if a method uses certain LDC values.
   * 
   * @param mn the method.
   * @param ldcValues the LDC values to check.
   * @return the result of this check.
   * 
   * @see org.objectweb.asm.tree.InsnList#accept(MethodVisitor)
   * @see org.objectweb.asm.tree.MethodNode#visitLdcInsn(Object)
   */
  public static boolean areLdcValuesUsed(MethodNode mn, final Object... ldcValues) {
    if (ldcValues == null || ldcValues.length == 0) {
      return false;
    }
    final List<Object> vals = new ArrayList<Object>();
    mn.instructions.accept(new MethodVisitor(Opcodes.ASM4) {
      @Override
      public void visitLdcInsn(Object cst) {
        for (Object o : ldcValues) {
          if (o.equals(cst)) {
            vals.add(cst);
            break;
          }
        }
        super.visitLdcInsn(cst);
      }
    });

    return vals.size() == ldcValues.length;
  }

  /**
   * Uses regular expressions to pull types out of a {@link String}. Converts from the format
   * Ljava/lang/Object; to the format java.lang.Object.
   * 
   * @param s the {@link String} that is being analyzed.
   * @return a list of types that were found in the supplied {@link String}.
   */
  public static List<String> pickOutTypes(String s) {
    if (s == null) {
      return new ArrayList<String>();
    }
    List<String> matches = new ArrayList<String>();
    Matcher m = Pattern.compile("L[.^;]*;").matcher(s.replace('/', '.'));
    while (m.find()) {
      s = m.group();
      if (s.indexOf(')') == -1) {
        matches.add(s.substring(1, s.length() - 1));
      }
    }
    return matches;
  }

  /**
   * A convenience method to get a {@link MethodNode} from a {@link ClassNode}.
   * 
   * @param classNode the {@link ClassNode} containing the requested {@link MethodNode}.
   * @param name the name of the requested method.
   * @param desc the descriptor/signature of the requested method.
   * @return the requested {@link MethodNode} or null if it was not found.
   * 
   * @see org.objectweb.asm.tree.ClassNode
   * @see org.objectweb.asm.tree.MethodNode
   * @see org.objectweb.asm.tree.ClassNode#methods
   * @see org.objectweb.asm.tree.MethodNode#desc
   * @see org.objectweb.asm.tree.MethodNode#name
   */
  public static MethodNode getMethodNode(ClassNode classNode, String name, String desc) {
    if (name != null && desc != null) {
      for (Object o : classNode.methods) {
        if (o instanceof MethodNode) {
          MethodNode m = (MethodNode) o;
          if (name.equals(m.name) && desc.equals(m.desc)) {
            return m;
          }
        }
      }
    }
    return null;
  }

  public static FieldNode getFieldNode(ClassNode classNode, String name) {
    if (classNode != null && name != null && name.length() > 0) {
      for (FieldNode f : getFields(classNode)) {
        if (f != null && f.name != null && f.name.equals(name)) {
          return f;
        }
      }
    }
    return null;
  }
}


/**
 * A class that probably already exists but I was too lazy to look for.
 * 
 * @author blootooby
 */
class StringIter {
  private String todo;
  private int curIndex;

  public StringIter(String todo, int startIndex) {
    this.todo = todo;
    this.curIndex = startIndex;
  }

  public StringIter(String todo) {
    this(todo, 0);
  }

  public void incrementIndex(int amount) {
    this.curIndex += amount;
  }

  public void incrementIndex() {
    this.incrementIndex(1);
  }

  public char getCurrentChar() {
    return this.todo.charAt(this.curIndex);
  }

  public String readUntil(char c) {
    String s = "";
    for (int i = this.curIndex; i < this.todo.length(); i++) {
      char x = this.todo.charAt(i);
      if (x == c) {
        return s;
      }
      s += x;
    }
    return s;
  }

  public String readUntilAndIncrement(char c) {
    String s = this.readUntil(c);
    this.incrementIndex(s.length());
    return s;
  }

  public char nextChar() {
    this.incrementIndex();
    return this.getCurrentChar();
  }

  public char peek() {
    return this.todo.charAt(this.curIndex + 1);
  }

  public char backtrack() {
    this.curIndex--;
    return this.todo.charAt(this.curIndex);
  }

  @Override
  public String toString() {
    return this.todo;
  }
}


/**
 * An {@link ArrayList} that doesn't store duplicates.
 * 
 * @author blootooby
 * 
 * @param <T> the type of object this list will contain.
 */
class NonDupList<T> extends ArrayList<T> {
  private static final long serialVersionUID = -8656629972970055500L;

  @Override
  public void add(int arg0, T arg1) {
    if (this.contains(arg1)) {
      return;
    }
    super.add(arg0, arg1);
  }

  @Override
  public boolean add(T arg0) {
    if (this.contains(arg0)) {
      return false;
    }
    return super.add(arg0);
  }
}
