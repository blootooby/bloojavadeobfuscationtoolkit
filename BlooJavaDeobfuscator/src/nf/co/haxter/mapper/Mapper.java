package nf.co.haxter.mapper;

import java.util.Collection;
import java.util.Map;

import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

/**
 * A mapper that maps out classes/fields/methods.
 * @author blootooby
 * @see MapperManager
 * @since July 14, 2014.
 */
public abstract class Mapper {
  /**
   * The manager for this Mapper object.
   * @see MapperManager
   * @see Mapper#setManager
   */
  protected MapperManager myManager;
  
  /**
   * The name to map this as. Varies with type. If this maps a class, it would be [ClassName]; if
   * this maps a method, it would be [ClassName].[MethodName][MethodDescriptor]; and if this maps a
   * field, it would be [ClassName].[FieldName]
   */
  protected String deobfuscatedName;
  
  /**
   * The prerequisites for this mapper to be able to work. These would be mappings needed before
   * this mapping can successfully run.
   */
  protected String[] prerequisites;


  /**
   * Creates a mapper for some obfuscated thing. (class, class member).
   * 
   * @param deobfuscatedName Fully qualified name to use when finished. For classes, this would be
   *        packagename.ClassName; for methods, packagename.ClassName.methodNameAndSignature; for
   *        fields, packagename.ClassName.fieldName.
   * @param prerequisites Anything that needs to be deobfuscated before being able to run this
   *        mapper. If this is mapping a class member, then the class may need to be specified as
   *        well.
   */
  public Mapper(String deobfuscatedName, String... prerequisites) {
    this.deobfuscatedName = deobfuscatedName;
    this.prerequisites = prerequisites;
  }

  /**
   * Checks if this mapper's prerequisites have been met.
   * @param currentMappings Mappings that have already been made, which includes class mappings, field mappings, and method mappings.
   * @return If all prerequisites for this mapper have been met.
   */
  public boolean arePrerequisitesMet(Collection<String> currentMappings) {
    for (String s : prerequisites) {
      if (!currentMappings.contains(s)) {
        return false;
      }
    }

    return true;
  }

  /**
   * Attempts to run this mapper. No specific exception is made here because we don't know what
   * they've put in this abstract method.
   * 
   * @param currentMappings
   * @throws Exception
   * 
   * @see org.objectweb.asm.tree.ClassNode
   * @see org.objectweb.asm.tree.MethodNode
   * @see org.objectweb.asm.tree.FieldNode
   */
  public abstract void run(Collection<String> classesToSearch, Map<String, ClassNode> classMap, Map<String, FieldNode> fieldMap,
      Map<String, MethodNode> methodMap) throws Exception;
  
  /**
   * Sets the {@link MapperManager} for this Mapper.
   * @param manager the {@link MapperManager} that manages this Mapper.
   * @see MapperManager
   * @see Mapper#myManager
   */
  public void setManager(MapperManager manager) {
    this.myManager = manager;
  }
  
  /**
   * A method where you can add all your member mappers.
   */
  public void addSubMappers() {}
}
