package nf.co.haxter.mapper;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;
/**
 * A manager for class/method/field mappers.
 * @author blootooby
 * @since July 14, 2014.
 * @see Mapper
 * @see org.objectweb.asm.tree.ClassNode
 * @see org.objectweb.asm.tree.MethodNode
 * @see org.objectweb.asm.tree.FieldNode
 */
public class MapperManager {
  /**
   * The list of Mappers that will be run when the runMappers() method is invoked.
   * 
   * @see Mapper
   * @see MapperManager#runMappers()
   */
  protected List<Mapper> mappers = new ArrayList<Mapper>();
  /**
   * The current method mappings.
   * 
   * @see org.objectweb.asm.tree.MethodNode
   */
  public Map<String, MethodNode> methodMappings = new HashMap<String, MethodNode>();
  /**
   * The current field mappings.
   * 
   * @see org.objectweb.asm.tree.FieldNode
   */
  public Map<String, FieldNode> fieldMappings = new HashMap<String, FieldNode>();
  /**
   * The current class mappings.
   * 
   * @see org.objectweb.asm.tree.ClassNode
   */
  public Map<String, ClassNode> classMappings = new HashMap<String, ClassNode>();

  /**
   * Map as [ClassName]
   * 
   * @param name name to be mapped as.
   * @param node node being mapped.
   * @see org.objectweb.asm.tree.ClassNode
   */
  public void mapNode(String name, ClassNode node) {
    this.classMappings.put(name, node);
  }

  /**
   * Map as [ClassName].[FieldName]
   * 
   * @param name name to be mapped as.
   * @param node node being mapped.
   * @see org.objectweb.asm.tree.FieldNode
   */
  public void mapNode(String name, FieldNode node) {
    this.fieldMappings.put(name, node);
  }

  /**
   * Map as [ClassName].[MethodName][MethodDescriptor/Signature]
   * 
   * @param name name to be mapped as.
   * @param node node being mapped.
   * @see org.objectweb.asm.tree.MethodNode
   */
  public void mapNode(String name, MethodNode node) {
    this.methodMappings.put(name, node);
  }

  /**
   * Runs the mappers that have been added until no more mappings can be made.
   * 
   * @see Mapper
   */
  public void runMappers(Collection<String> classesToSearch) {
    Set<Mapper> toBeRemoved = new HashSet<Mapper>();
    while (!this.mappers.isEmpty()) {
      for (Mapper mapper : this.mappers) {
        Collection<String> s = new HashSet<String>();
        s.addAll(this.classMappings.keySet());
        s.addAll(this.fieldMappings.keySet());
        s.addAll(this.methodMappings.keySet());
        if (mapper.arePrerequisitesMet(s)) {
          boolean foundHook = false;
          try {
            int size = this.classMappings.size() + this.fieldMappings.size() + this.methodMappings.size();
            mapper.run(classesToSearch, this.classMappings, this.fieldMappings, this.methodMappings);
            foundHook = size != this.classMappings.size() + this.fieldMappings.size() + this.methodMappings.size();
          } catch (Exception e) {
            e.printStackTrace();
          }
          if (foundHook) {
            toBeRemoved.add(mapper);
          }
        } else {
          continue;
        }
      }

      if (toBeRemoved.isEmpty()) {
        break;
      } else {
        this.mappers.removeAll(toBeRemoved);
        toBeRemoved.clear();
      }
    }
  }

  /**
   * A convenience method that gets a {@link Field} from the current field mappings.
   * 
   * @param clazz the {@link Class} that contains the {@link Field}.
   * @param deobfName the de-obfuscated name of the {@link Field}.
   * @return the {@link Field} that you are looking for.
   * @throws NoSuchFieldException when the field has not been mapped or the deobfuscated name is
   *         incorrect.
   * @throws SecurityException when reflection is not supported by the {@link SecurityManager}.
   * @see org.objectweb.asm.tree.FieldNode
   * @see java.lang.reflect.Field
   */
  public Field getField(Class<?> clazz, String deobfName) throws NoSuchFieldException,
      SecurityException {
    return clazz.getDeclaredField(this.fieldMappings.get(deobfName).name);
  }

  /**
   * A convenience method that gets a {@link Class} from the current class mappings.
   * 
   * @param cl the {@link ClassLoader} that will load the mapped {@link Class}.
   * @param deobfName the de-obfuscated name of the {@link Class}.
   * @return the mapped {@link Class}.
   * @throws ClassNotFoundException when the provided {@link ClassLoader} is unable to find the
   *         mapped {@link Class}.
   * @see Mapper
   * @see Class
   * @see ClassLoader#loadClass(String)
   * @see org.objectweb.asm.tree.ClassNode
   * @see org.objectweb.asm.tree.ClassNode#name
   */
  public Class<?> getClass(ClassLoader cl, String deobfName) throws ClassNotFoundException {
    return cl.loadClass(this.classMappings.get(deobfName).name.replace('/', '.'));
  }

  /**
   * A convenience method that gets a {@link Method} from the current method mappings.
   * 
   * @param cl a {@link ClassLoader} to parse method parameter types.
   * @param clazz the {@link Class} that contains the {@link Method}.
   * @param deobf the de-obfuscated method name and method descriptor.
   * @return the mapped {@link Method}.
   * @throws NoSuchMethodException when the {@link Method} is not mapped or the mapping is
   *         incorrect.
   * @throws SecurityException when reflection is not supported by the {@link SecurityManager}.
   * @throws ClassNotFoundException when the parsed parameter types cannot be found.
   * @see Mapper
   * @see ClassLoader#loadClass(String)
   * @see org.objectweb.asm.tree.MethodNode
   * @see org.objectweb.asm.tree.MethodNode#name
   * @see org.objectweb.asm.tree.MethodNode#desc
   */
  public Method getMethod(ClassLoader cl, Class<?> clazz, String deobf)
      throws NoSuchMethodException, SecurityException, ClassNotFoundException {
    // FIXME MAYBE

    MethodNode mn = this.methodMappings.get(clazz.getName() + '.' + deobf);
    return clazz.getDeclaredMethod(mn.name,
        MappingUtils.parseParameterTypesFromMethodSignature(classMappings, cl, mn.desc));
  }
  
  /**
   * Removes all mappings, including class mappings, field mappings, and method mappings.
   */
  public void clearAllMappings() {
    this.fieldMappings.clear();
    this.classMappings.clear();
    this.methodMappings.clear();
  }
  
  /**
   * Adds a {@link Mapper} to this manager.
   * @param mapper
   */
  public void addMapper(Mapper mapper) {
    if (mapper == null) {
      return;
    }
    mapper.myManager = this;
    this.mappers.add(mapper);
    mapper.addSubMappers();
  }
}
