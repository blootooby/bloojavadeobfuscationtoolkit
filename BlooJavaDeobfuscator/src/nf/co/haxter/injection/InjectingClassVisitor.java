package nf.co.haxter.injection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import nf.co.haxter.injection.mod.ClassMod;
import nf.co.haxter.injection.mod.FieldMod;
import nf.co.haxter.injection.mod.MethodMod;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
/**
 * A class-visitor for using {@link FieldMod}s and {@link MethodMod}s.
 * @author blootooby
 * @since July 15, 2014.
 */
public class InjectingClassVisitor extends ClassVisitor implements Opcodes {
  
  public InjectingClassVisitor(Collection<ClassMod> mods, ClassVisitor cv) {
    super(ASM4, cv);
    this.shovelIn(mods);
  }

  public InjectingClassVisitor(Collection<ClassMod> mods) {
    super(ASM4);
    this.shovelIn(mods);
  }

  @Override
  public FieldVisitor visitField(int access, String name, String desc, String signature,
      Object value) {
    if (!this.shouldIncludeField(access, name, desc, signature, value)) {
      return null;
    } else {
      for (FieldMod f : this.fieldMods) {
        if (f.modType == FieldMod.ModType.EDIT) {
          return cv.visitField(f.getChangedModifiers(), f.getChangedName(), f.getChangedDescriptor(), f.getChangedSignature(), f.getChangedInitialValue());
        }
      }
    }
    
    // DEBUG
    access &= ~Opcodes.ACC_PRIVATE;
    access &= ~Opcodes.ACC_PROTECTED;
    access &= ~Opcodes.ACC_PUBLIC;
    access |= Opcodes.ACC_PUBLIC;
    // DEBUG
    
    return super.visitField(access, name, desc, signature, value);
  }
  
  @Override
  public MethodVisitor visitMethod(int access, String name, String desc, String signature,
      String[] exceptions) {
    if (!shouldIncludeMethod(access, name, desc, signature, exceptions)) {
      return null;
    }
    return makeEdits(access, name, desc, signature, exceptions, this);
  }
  
  @Override
  public void visitEnd() {
    this.makeAdditions(this);
    super.visitEnd();
  }
  
  
  private List<FieldMod> fieldMods = new ArrayList<FieldMod>();
  private List<MethodMod> methodMods = new ArrayList<MethodMod>();

  private void shovelIn(Collection<ClassMod> pile) {
    for (ClassMod mod : pile) {
      if (mod == null) {
        continue;
      }

      if (mod instanceof FieldMod) {
        fieldMods.add((FieldMod) mod);
      } else if (mod instanceof MethodMod) {
        methodMods.add((MethodMod) mod);
      }
    }
  }

  public boolean shouldIncludeField(int access, String name, String desc, String signature,
      Object value) {
    for (FieldMod mod : this.fieldMods) {
      if (mod.equals(access, name, desc, signature, value)
          && mod.modType == FieldMod.ModType.DELETION) {
        return false;
      }
    }
    return true;
  }

  public boolean shouldIncludeMethod(int access, String name, String desc, String signature,
      String[] exceptions) {
    for (MethodMod mod : this.methodMods) {
      if (mod.equals(access, name, desc, signature, exceptions)
          && mod.modType == MethodMod.ModType.DELETION) {
        return false;
      }
    }
    return true;
  }

  public void makeAdditions(ClassVisitor cv) {
    for (FieldMod mod : this.fieldMods) {
      if (mod.modType == FieldMod.ModType.ADDITION) {
        cv.visitField(mod.modifiers, mod.name, mod.desc, mod.signature, mod.value);
      }
    }
    for (MethodMod mod : this.methodMods) {
      if (mod.modType == MethodMod.ModType.ADDITION) {
        MethodVisitor mv = cv.visitMethod(mod.modifiers, mod.name, mod.desc, mod.signature, mod.exceptions);
        mv.visitCode();
        if (mod.instructions != null) {
          for (AbstractInsnNode ain : mod.instructions) {
            ain.accept(mv);
          }
        }
        mod.addLocalVariables(mv);
        mv.visitMaxs(0, 0);
        mv.visitEnd();
      }
    }
  }
  
  public MethodVisitor makeEdits(int access, String name, String desc, String signature, String[] exceptions, ClassVisitor cv) {
    MethodVisitor mv = super.visitMethod(access, name, desc, signature, exceptions);
    List<MethodMod> edits = new ArrayList<MethodMod>();
    for (MethodMod mod : this.methodMods) {
      if (mod.modType == MethodMod.ModType.EDIT && mod.equals(access, name, desc, signature, exceptions)) {
        edits.add(mod);
      }
    }
    if (edits.isEmpty()) {
      return mv;
    }
    this.methodMods.removeAll(edits);
    return new InjectingMethodVisitor(edits, mv);
  }
  
}