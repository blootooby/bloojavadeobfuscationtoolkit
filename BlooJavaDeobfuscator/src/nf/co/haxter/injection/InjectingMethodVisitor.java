package nf.co.haxter.injection;

import java.util.List;

import nf.co.haxter.injection.mod.MethodMod;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
/**
 * A method-visitor for using {@link MethodMod}s.
 * @author blootooby
 * @see MethodMod
 * @see InjectingClassVisitor
 */
public class InjectingMethodVisitor extends MethodVisitor implements Opcodes {
  private List<MethodMod> edits;
  private int line0 = -1;

  public InjectingMethodVisitor(List<MethodMod> editsToMake, MethodVisitor mv) {
    super(ASM4, mv);
    this.edits = editsToMake;
  }

  public InjectingMethodVisitor(List<MethodMod> editsToMake) {
    super(ASM4);
    this.edits = editsToMake;
  }

  @Override
  public void visitLineNumber(int line, Label start) {
    if (line0 == -1) {
      line0 = line;
    }
    super.visitLineNumber(line, start);
    for (MethodMod mod : this.edits) {
      if (mod.editRangeStart == line - line0) {
        for (AbstractInsnNode ain : mod.instructions) {
          ain.accept(this);
        }
      }
    }
  }
}
