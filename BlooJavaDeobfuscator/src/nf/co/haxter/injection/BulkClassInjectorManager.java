package nf.co.haxter.injection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import nf.co.haxter.injection.mod.ClassMod;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
/**
 * A simple manager for class injections.
 * @author blootooby
 * @since July 15, 2014.
 */
public class BulkClassInjectorManager {
  protected Map<String, Collection<ClassMod>> allMods = new HashMap<String, Collection<ClassMod>>();

  /**
   * Adds modifications to a class.
   * @param clazz the name of the class.
   * @param edits the modifications to add.
   */
  public void addMods(String clazz, Collection<ClassMod> edits) {
    Collection<ClassMod> x = this.allMods.get(clazz);
    if (x != null) {
      x.addAll(edits);
    } else {
      this.allMods.put(clazz, edits);
    }
  }
  
  /**
   * Adds modifications to a class.
   * @param clazz the name of the class.
   * @param edits the modifications to add.
   */
  public void addMods(String clazz, ClassMod...edits) {
    Collection<ClassMod> x = this.allMods.get(clazz);
    if (x != null) {
      for (ClassMod mod : edits) {
        x.add(mod);
      }
    } else {
      x = new ArrayList<ClassMod>(edits.length);
      for (ClassMod mod : edits) {
        x.add(mod);
      }
      this.allMods.put(clazz, x);
    }
  }
  
  /**
   * Gets the modifications for the passed-in class.
   * @param clazz the name of the class.
   * @return the modifications for this class.
   */
  public Collection<ClassMod> getMods(String clazz) {
    return this.allMods.get(clazz);
  }
  
  public byte[] getFinishedClassBytecode(String clazz, ClassReader cr) {
    Collection<ClassMod> mods = this.allMods.get(clazz);
    ClassInjector c = new ClassInjector(cr);
    for (ClassMod m : mods) {
      c.createEdit(m);
    }
    return c.finalizeClass(0, ClassWriter.COMPUTE_MAXS);
  }
}
