package nf.co.haxter.injection.mod;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import nf.co.haxter.injection.ClassInjector;
import nf.co.haxter.util.BytecodeUtils;

import org.objectweb.asm.Label;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.LocalVariableNode;
import org.objectweb.asm.tree.VarInsnNode;

/**
 * A modification to a class file which is injected into said class file.
 * 
 * @author blootooby
 * @see ClassInjector
 * @see ClassModManager
 * @see FieldMod
 * @see MethodMod
 */
public abstract class ClassMod implements Opcodes {
  /**
   * The package of the class being modified, separated by '.'s.
   */
  protected String classPackage;
  /**
   * The name of the class being modified, excluding the class package.
   */
  protected String className;

  /**
   * Creates a modification for a class.
   * 
   * @param fullyQualifiedClassName the fully qualified name of the class.
   */
  public ClassMod(String fullyQualifiedClassName) {
    int i = fullyQualifiedClassName.lastIndexOf('.');
    fullyQualifiedClassName = fullyQualifiedClassName.replace('/', '.');
    this.classPackage = fullyQualifiedClassName.substring(0, i);
    this.className = fullyQualifiedClassName.substring(i + 1);
  }

  /**
   * Creates a modification for a class.
   * 
   * @param classPackage the package of the class.
   * @param className the name of the class, excluding the package.
   */
  public ClassMod(String classPackage, String className) {
    this.classPackage = classPackage.replace('/', '.');
    this.className = className;
  }

  /**
   * Generates a {@link MethodMod} that adds a getter for a field to its class. The modifier for the
   * generated getter is, by default, public.
   * 
   * @param clazz the class being modified, represented by a {@link ClassNode}.
   * @param field the field that this getter is being generated for, represented by a
   *        {@link FieldNode}.
   * @param newFieldName the name for this getter, which will make the getter's name in the format
   *        of get[name], where the first letter of [name] is capitalized.
   * @return the generated {@link MethodMod} for the getter.
   */
  public static MethodMod createGetter(ClassNode clazz, FieldNode field, String newFieldName) {
    String n = clazz.name.replace('/', '.');
    MethodMod m =
        new MethodMod(BytecodeUtils.getPackageName(n), BytecodeUtils.getClassName(n), ACC_PUBLIC,
            "get" + ("" + newFieldName.charAt(0)).toUpperCase() + newFieldName.substring(1), "()"
                + field.desc, field.signature, null, MethodMod.ModType.ADDITION);
    m.instructions.add(new VarInsnNode(ALOAD, 0));
    m.instructions.add(new FieldInsnNode(GETFIELD, clazz.name, field.name, field.desc));
    Type t = Type.getType(field.desc);
    m.instructions.add(new InsnNode(t.getOpcode(IRETURN)));
    return m;
  }

  /**
   * Generates a {@link MethodMod} that adds a getter for a static field to its class. The modifiers
   * for the generated getter are, by default, public and static.
   * 
   * @param clazz the class being modified, represented by a {@link ClassNode}.
   * @param staticField the static field that this getter is being generated for, represented by a
   *        {@link FieldNode}.
   * @param newFieldName the name for this getter, which will make the getter's name in the format
   *        of get[name], where the first letter of [name] is capitalized.
   * @return the generated {@link MethodMod} for the getter.
   */
  public static MethodMod createStaticGetter(ClassNode clazz, FieldNode staticField,
      String newFieldName) {
    String n = clazz.name.replace('/', '.');

    MethodMod m =
        new MethodMod(BytecodeUtils.getPackageName(n), BytecodeUtils.getClassName(n), ACC_PUBLIC
            + ACC_STATIC, "get" + ("" + newFieldName.charAt(0)).toUpperCase()
            + newFieldName.substring(1), "()" + staticField.desc, staticField.signature, null,
            MethodMod.ModType.ADDITION);
    m.instructions
        .add(new FieldInsnNode(GETSTATIC, clazz.name, staticField.name, staticField.desc));
    //char c = staticField.desc.charAt(0);
    Type t = Type.getType(staticField.desc);
    m.instructions.add(new InsnNode(t.getOpcode(IRETURN)));
    /*AbstractInsnNode rn = null;
    switch (c) {
      case 'Z':
      case 'S':
      case 'B':
      case 'C':
      case 'I':
        rn = new InsnNode(IRETURN);
        break;
      case 'J':
        rn = new InsnNode(LRETURN);
        break;
      case 'D':
        rn = new InsnNode(DRETURN);
        break;
      case 'F':
        rn = new InsnNode(FRETURN);
        break;
      case 'L':
        rn = new InsnNode(ARETURN);
        break;
    }
    if (rn == null) {
      new Exception("Couldn't identify a return type! Exiting now, as it could be fatal later!")
          .printStackTrace();
      System.exit(-1);
    }
    m.instructions.add(rn);*/
    return m;
  }

  public static MethodMod createSetter(ClassNode clazz, FieldNode field, String newFieldName) {
    String n = clazz.name.replace('/', '.');

    MethodMod m =
        new MethodMod(BytecodeUtils.getPackageName(n), BytecodeUtils.getClassName(n), ACC_PUBLIC,
            "set" + ("" + newFieldName.charAt(0)).toUpperCase() + newFieldName.substring(1), '('
                + field.desc + ")V", field.signature, null, MethodMod.ModType.ADDITION); // TODO proper signatures?
    Type t = Type.getType(field.desc);
    //LabelNode l0 = new LabelNode(new Label());
    //m.instructions.add(l0);
    m.instructions.add(new VarInsnNode(ALOAD, 0));
    m.instructions.add(new VarInsnNode(t.getOpcode(ILOAD), 1));
    m.instructions.add(new FieldInsnNode(PUTFIELD, clazz.name, field.name, field.desc));
    //LabelNode l1 = new LabelNode(new Label());
    //m.instructions.add(l1);
    m.instructions.add(new InsnNode(RETURN));
    //LabelNode l2 = new LabelNode(new Label());
    //m.instructions.add(l2);
    //m.localVarNodes.add(new LocalVariableNode("this", 'L' + clazz.name + ';', null, l0, l2, 0));
    //m.localVarNodes.add(new LocalVariableNode(newFieldName, field.desc, null, l0, l2, 1));
    return m;
  }
  
  public static MethodMod createStaticSetter(ClassNode clazz, FieldNode staticField, String newFieldName) {
    String n = clazz.name.replace('/', '.');
    
    MethodMod m =
        new MethodMod(BytecodeUtils.getPackageName(n), BytecodeUtils.getClassName(n), ACC_PUBLIC + ACC_STATIC,
            "set" + ("" + newFieldName.charAt(0)).toUpperCase() + newFieldName.substring(1), '('
            + staticField.desc + ")V", staticField.signature, null, MethodMod.ModType.ADDITION); // TODO proper signatures?
    Type t = Type.getType(staticField.desc);
    //LabelNode l0 = new LabelNode(new Label());
    //m.instructions.add(l0);
    m.instructions.add(new VarInsnNode(t.getOpcode(ILOAD), 0));
    m.instructions.add(new FieldInsnNode(PUTSTATIC, clazz.name, staticField.name, staticField.desc));
    //LabelNode l1 = new LabelNode(new Label());
    //m.instructions.add(l1);
    m.instructions.add(new InsnNode(RETURN));
    //LabelNode l2 = new LabelNode(new Label());
    //m.instructions.add(l2);
    //m.localVarNodes.add(new LocalVariableNode(newFieldName, staticField.desc, null, l0, l2, 0));
    return m;
  }

  public String getClassName() {
    return this.className;
  }

  public String getPackageName() {
    return this.classPackage;
  }
  
  public void setClassName(String s) {
    this.className = s;
  }
  
  public static float apple;
  public static void setApple(float apple) {
    ClassMod.apple = apple;
  }

  public String getFullyQualifiedClassName() {
    return this.classPackage + '.' + this.className;
  }
}
