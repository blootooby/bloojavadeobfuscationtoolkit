package nf.co.haxter.injection.mod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import nf.co.haxter.injection.InjectingClassVisitor;
import nf.co.haxter.util.BytecodeUtils;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.LocalVariableNode;
import org.objectweb.asm.tree.MethodNode;

public class MethodMod extends ClassMod {
  public int modifiers;
  public String name;
  public String desc;
  public String signature;
  public String[] exceptions;
  public final ModType modType;

  public int editRangeStart;
  public List<AbstractInsnNode> instructions = new ArrayList<AbstractInsnNode>();
  public List<LocalVariableNode> localVarNodes = new ArrayList<LocalVariableNode>();

  public MethodMod(String classPackage, String className, int modifiers, String name, String desc,
      String signature, String[] exceptions, ModType modType) {
    super(classPackage, className);
    this.modifiers = modifiers;
    this.name = name;
    this.desc = desc;
    this.signature = signature;
    this.exceptions = exceptions;
    this.modType = modType;
  }

  public boolean equals(int modifiers, String name, String desc, String signature,
      String[] exceptions) {
    return this.modifiers == modifiers
        && this.check(this.name, name)
        && this.check(this.desc, desc)
        && this.check(this.signature, signature);
    //    && (this.check(this.exceptions, exceptions) || Arrays.equals(this.exceptions, exceptions) || Arrays
    //        .deepEquals(this.exceptions, exceptions)); FIXME
  }

  public void addLocalVariables(MethodVisitor mv) {
    for (LocalVariableNode lvn : this.localVarNodes) {
      lvn.accept(mv);
    }
  }

  private boolean check(Object o, Object o1) {
    return o == o1 || (o != null && o.equals(o1));
  }

  public static enum ModType {
    ADDITION, DELETION, EDIT
  }

  /**
   * Creates a modification of the supplied method (represented by the passed-in {@link MethodNode})
   * in which it is removed from the final version of the class (represented by the passed-in
   * {@link ClassNode}).
   * 
   * @param cn the class in which the method will be removed.
   * @param mn a representation of the method which will be removed.
   * @return the modification created.
   * @see MethodMod
   * @see MethodMod#MethodMod(String, String, int, String, String, String, String[], ModType)
   * @see InjectingClassVisitor
   * @see org.objectweb.asm.tree.ClassNode
   * @see org.objectweb.asm.tree.ClassNode#name
   * @see org.objectweb.asm.tree.MethodNode
   * @see org.objectweb.asm.tree.MethodNode#access
   * @see org.objectweb.asm.tree.MethodNode#name
   * @see org.objectweb.asm.tree.MethodNode#desc
   * @see org.objectweb.asm.tree.MethodNode#signature
   * @see org.objectweb.asm.tree.MethodNode#exceptions
   */
  public static MethodMod createDeletionEdit(ClassNode cn, MethodNode mn) {
    return new MethodMod(BytecodeUtils.getPackageName(cn.name),
        BytecodeUtils.getPackageName(cn.name), mn.access, mn.name, mn.desc, mn.signature,
        ((List<String>) mn.exceptions).toArray(new String[mn.exceptions.size()]), ModType.DELETION);
  }

  /**
   * Creates a modification of the supplied method (represented by the passed-in {@link MethodNode})
   * in which it is edited from the original version of the class (represented by the passed-in
   * {@link ClassNode}).
   * 
   * @param cn the class in which the method will be edited.
   * @param mn a representation of the method which will be edited.
   * @return the modification created.
   * @see MethodMod
   * @see MethodMod#MethodMod(String, String, int, String, String, String, String[], ModType)
   * @see InjectingClassVisitor
   * @see org.objectweb.asm.tree.ClassNode
   * @see org.objectweb.asm.tree.ClassNode#name
   * @see org.objectweb.asm.tree.MethodNode
   * @see org.objectweb.asm.tree.MethodNode#access
   * @see org.objectweb.asm.tree.MethodNode#name
   * @see org.objectweb.asm.tree.MethodNode#desc
   * @see org.objectweb.asm.tree.MethodNode#signature
   * @see org.objectweb.asm.tree.MethodNode#exceptions
   */
  public static MethodMod createEdit(ClassNode cn, MethodNode mn) {
    return new MethodMod(BytecodeUtils.getPackageName(cn.name),
        BytecodeUtils.getPackageName(cn.name), mn.access, mn.name, mn.desc, mn.signature,
        ((List<String>) mn.exceptions).toArray(new String[mn.exceptions.size()]), ModType.EDIT);
  }

  /**
   * Creates a modification of the supplied class (represented by the passed-in {@link ClassNode})
   * in which a new method is added.
   * 
   * @param cn the class in which the method will be edited.
   * @param modifiers the modifiers of the method.
   * @param name the method's name.
   * @param desc the method's descriptor.
   * @param signature the method's signature.
   * @param exceptions the exceptions which the method may throw.
   * @return the modification created.
   * @see MethodMod
   * @see MethodMod#MethodMod(String, String, int, String, String, String, String[], ModType)
   * @see InjectingClassVisitor
   * @see org.objectweb.asm.tree.ClassNode
   * @see org.objectweb.asm.tree.ClassNode#name
   * @see org.objectweb.asm.tree.MethodNode
   * @see org.objectweb.asm.tree.MethodNode#access
   * @see org.objectweb.asm.tree.MethodNode#name
   * @see org.objectweb.asm.tree.MethodNode#desc
   * @see org.objectweb.asm.tree.MethodNode#signature
   * @see org.objectweb.asm.tree.MethodNode#exceptions
   */
  public static MethodMod createAdditionEdit(ClassNode cn, int modifiers, String name, String desc,
      String signature, String[] exceptions) {
    return new MethodMod(BytecodeUtils.getPackageName(cn.name),
        BytecodeUtils.getPackageName(cn.name), modifiers, name, desc, signature, exceptions,
        ModType.ADDITION);
  }
}
