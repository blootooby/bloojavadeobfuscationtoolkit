package nf.co.haxter.injection.mod;

import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;

/**
 * A modification to a field. Supports adding, removing, and editing fields.
 * 
 * @author blootooby
 */
public class FieldMod extends ClassMod {
  /**
   * The modifiers of the field.
   */
  public int modifiers;

  /**
   * The name of the field.
   */
  public String name;

  /**
   * The descriptor of the field.
   */
  public String desc;

  /**
   * The signature of the field.
   */
  public String signature;

  /**
   * The initial value of the field.
   */
  public Object value;

  /**
   * The type of modification being made.
   */
  public ModType modType;

  /*
   * these values below are only used if the modification type is EDIT
   */
  private int changedModifiers;
  private String changedName;
  private String changedDescriptor;
  private String changedSignature;
  private Object changedValue;

  /**
   * Creates a field modification.
   * 
   * @param fullyQualifiedClassName the fully qualified name of the field's owner class.
   * @param modifiers the modifiers of the field.
   * @param name the name of the field.
   * @param desc the descriptor of the field.
   * @param signature the signature of the field.
   * @param value the initial value of the field.
   * @param modType the type of modification being made.
   * @see FieldMod.ModType
   */
  public FieldMod(String fullyQualifiedClassName, int modifiers, String name, String desc,
      String signature, Object value, ModType modType) {
    super(fullyQualifiedClassName);
    this.modifiers = modifiers;
    this.name = name;
    this.desc = desc;
    this.signature = signature;
    this.value = value;
    
    this.changedModifiers = modifiers;
    this.changedName = name;
    this.changedDescriptor = desc;
    this.changedSignature = signature;
    this.changedValue = value;
    
    this.modType = modType;
  }

  /**
   * Creates a field modification.
   * 
   * @param classPackage the package of the field's owner class.
   * @param className the name of the field's owner class, excluding the package name.
   * @param modifiers the modifiers of the field.
   * @param name the name of the field.
   * @param desc the descriptor of the field.
   * @param signature the signature of the field.
   * @param value the initial value of the field.
   * @param modType the type of modification being made.
   * @see FieldMod.ModType
   */
  public FieldMod(String classPackage, String className, int modifiers, String name, String desc,
      String signature, Object value, ModType modType) {
    super(classPackage, className);
    this.modifiers = modifiers;
    this.name = name;
    this.desc = desc;
    this.signature = signature;
    this.value = value;
    
    this.changedModifiers = modifiers;
    this.changedName = name;
    this.changedDescriptor = desc;
    this.changedSignature = signature;
    this.changedValue = value;
    
    this.modType = modType;
  }

  /**
   * Checks if the supplied properties match those of this modification. Only used for edit or
   * deletion modifications.
   * 
   * @param modifiers the modifiers of the field.
   * @param name the name of the field.
   * @param desc the descriptor of the field.
   * @param signature the signature of the field.
   * @param value the initial value of the field.
   * @return if the properties match those of this modification.
   */
  public boolean equals(int modifiers, String name, String desc, String signature, Object value) {
    return this.modifiers == modifiers && this.check(this.name, name)
        && this.check(this.desc, desc) && this.check(this.signature, signature)
        && this.check(this.value, value);
  }

  public void changeName(String name) {
    if (this.modType == ModType.EDIT) {
      this.changedName = name;
    } else if (this.modType == ModType.ADDITION) {
      this.name = name;
    }
  }

  public void changeDescriptor(String desc) {
    if (this.modType == ModType.EDIT) {
      this.changedDescriptor = desc;
    } else if (this.modType == ModType.ADDITION) {
      this.desc = desc;
    }
  }

  public void changeSignature(String signature) {
    if (this.modType == ModType.EDIT) {
      this.changedSignature = signature;
    } else if (this.modType == ModType.ADDITION) {
      this.signature = signature;
    }
  }

  public void changeInitialValue(Object value) {
    if (this.modType == ModType.EDIT) {
      this.changedValue = value;
    } else if (this.modType == ModType.ADDITION) {
      this.value = value;
    }
  }
  
  
  
  public int getChangedModifiers() {
    return changedModifiers;
  }

  public String getChangedName() {
    return changedName;
  }

  public String getChangedDescriptor() {
    return changedDescriptor;
  }

  public String getChangedSignature() {
    return changedSignature;
  }

  public Object getChangedInitialValue() {
    return changedValue;
  }
  

  private boolean check(Object o, Object o1) {
    return o == o1 || (o != null && o.equals(o1));
  }

  /**
   * Types of possible field modifications.
   * 
   * @author blootooby
   */
  public static enum ModType {
    ADDITION, DELETION, EDIT
  }

  /**
   * A convenience method to instantiate a {@link FieldMod} from a {@link ClassNode} and
   * {@link FieldNode}.
   * 
   * @param originalClass a {@link ClassNode} representing the class being modified.
   * @param originalField a {@link FieldNode} representing the field being modified.
   * @param modType the type of modification the {@link FieldMod} will make.
   * @return the field modification.
   * @see FieldMod.ModType
   * @see org.objectweb.asm.tree.ClassNode
   * @see org.objectweb.asm.tree.FieldNode
   */
  public static FieldMod fromNodes(ClassNode originalClass, FieldNode originalField, ModType modType) {
    return new FieldMod(originalClass.name, originalField.access, originalField.name,
        originalField.desc, originalField.signature, originalField.value, modType);
  }
}
