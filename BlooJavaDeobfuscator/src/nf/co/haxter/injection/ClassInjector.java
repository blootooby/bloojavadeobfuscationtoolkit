package nf.co.haxter.injection;

import java.util.ArrayList;
import java.util.List;

import nf.co.haxter.injection.mod.ClassMod;
import nf.co.haxter.mapper.MappingUtils;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;

/**
 * A class to manage class injections.
 * @author blootooby
 * @since July 15, 2014.
 * @see ClassMod
 * @see MethodMod
 * @see FieldMod
 */
public final class ClassInjector {

  /**
   * Merely here for your convenience.
   * @see ClassWriter#COMPUTE_FRAMES
   */
  public static int WRITE_FLAG_COMPUTE_FRAMES = ClassWriter.COMPUTE_FRAMES;
  /**
   * Merely here for your convenience.
   * @see ClassWriter#COMPUTE_MAXS
   */
  public static int WRITE_FLAG_COMPUTE_MAXS = ClassWriter.COMPUTE_MAXS;
  /**
   * Merely here for your convenience.
   * @see ClassReader#EXPAND_FRAMES
   */
  public static int VISIT_FLAG_EXPAND_FRAMES = ClassReader.EXPAND_FRAMES;
  /**
   * Merely here for your convenience.
   * @see ClassReader#SKIP_CODE
   */
  public static int VISIT_FLAG_SKIP_CODE = ClassReader.SKIP_CODE;
  /**
   * Merely here for your convenience.
   * @see ClassReader#SKIP_DEBUG
   */
  public static int VISIT_FLAG_SKIP_DEBUG = ClassReader.SKIP_DEBUG;
  /**
   * Merely here for your convenience.
   * @see ClassReader#SKIP_FRAMES
   */
  public static int VISIT_FLAG_SKIP_FRAMES = ClassReader.SKIP_FRAMES;

  private ClassReader cr;
  private ClassNode cn;
  private boolean canEdit = true;

  private List<ClassMod> edits = new ArrayList<ClassMod>();

  public ClassInjector(ClassReader cr) {
    this.cr = cr;
  }

  public ClassInjector(ClassNode cn) {
    this.cn = cn;
  }

  public void createEdit(ClassMod edit) {
    if (this.canEdit && !this.edits.contains(edit)) {
      this.edits.add(edit);
    }
  }

  public byte[] finalizeClass(int visitFlags, int writeFlags) {
    this.canEdit = false;
    ClassWriter cw = new ClassWriter(cr, writeFlags);
    InjectingClassVisitor injector = new InjectingClassVisitor(this.edits, cw);
    if (this.cr != null) {
      this.cr.accept(injector, visitFlags);
    } else {
      this.cn.accept(injector);
    }
    return cw.toByteArray();
  }
}
