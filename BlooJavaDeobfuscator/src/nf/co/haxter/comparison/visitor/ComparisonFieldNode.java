package nf.co.haxter.comparison.visitor;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.FieldNode;

public class ComparisonFieldNode extends FieldNode implements Opcodes {

  // modifiers
  public boolean isPrivate;
  public boolean isPublic;
  public boolean isProtected;
  public boolean isStatic;
  public boolean isFinal;
  public boolean isVolatile;
  public boolean isStrict;
  public boolean isTransient;
  public boolean isSynthetic;
  public boolean isEnumConstant;
  
  public ComparisonFieldNode(int api, int access, String name, String desc, String signature,
      Object value) {
    super(api, access, name, desc, signature, value);
    this.checkAccessModifiers();
  }

  public ComparisonFieldNode(int access, String name, String desc, String signature, Object value) {
    super(access, name, desc, signature, value);
    this.checkAccessModifiers();
  }
  
  private void checkAccessModifiers() {
    this.isFinal = (this.access & ACC_FINAL) != 0;
    this.isStatic = (this.access & ACC_STATIC) != 0;
    this.isPublic = (this.access & ACC_PUBLIC) != 0;
    this.isPrivate = (this.access & ACC_PRIVATE) != 0;
    this.isProtected = (this.access & ACC_PROTECTED) != 0;
    this.isStrict = (this.access & ACC_STRICT) != 0;
    this.isVolatile = (this.access & ACC_VOLATILE) != 0;
    this.isSynthetic = (this.access & ACC_SYNTHETIC) != 0;
    this.isTransient = (this.access & ACC_TRANSIENT) != 0;
    this.isEnumConstant = (this.access & ACC_ENUM) != 0;
  }

}
