package nf.co.haxter.comparison.visitor;

import java.util.ArrayList;
import java.util.List;

import javax.lang.model.element.Modifier;

import org.objectweb.asm.Handle;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.MethodNode;

public class ComparisonMethodNode extends MethodNode implements Opcodes {
  private static int[] possibleBitMasks = new int[] {ACC_ABSTRACT, ACC_PUBLIC, ACC_PRIVATE, ACC_PROTECTED, ACC_FINAL, ACC_STATIC, ACC_STRICT, ACC_NATIVE, ACC_TRANSIENT, ACC_SYNCHRONIZED, ACC_SYNTHETIC};
  
  // modifiers
  public boolean isPrivate;
  public boolean isPublic;
  public boolean isProtected;
  public boolean isStatic;
  public boolean isFinal;
  public boolean isAbstract;
  public boolean isSynchronized;
  public boolean isNative;
  public boolean isStrict;
  public boolean isTransient;
  public boolean isSynthetic;
  
  // sorted uses of LDC constant values
  public List<Object> ldcValues = new ArrayList<Object>();
  public List<String> ldcStringValues = new ArrayList<String>();
  public List<Float> ldcFloatValues = new ArrayList<Float>();
  public List<Integer> ldcIntValues = new ArrayList<Integer>();
  public List<Double> ldcDoubleValues = new ArrayList<Double>();
  public List<Long> ldcLongValues = new ArrayList<Long>();
  public List<Type> ldcObjectTypeValues = new ArrayList<Type>();
  public List<Type> ldcArrayTypeValues = new ArrayList<Type>();
  public List<Type> ldcMethodTypeValues = new ArrayList<Type>();
  public List<Handle> ldcHandleValues = new ArrayList<Handle>();

  
  
  public ComparisonMethodNode() {
    super();
    this.checkAccessModifiers();
  }

  public ComparisonMethodNode(int api, int access, String name, String desc, String signature,
      String[] exceptions) {
    super(api, access, name, desc, signature, exceptions);
    this.checkAccessModifiers();
  }

  public ComparisonMethodNode(int access, String name, String desc, String signature,
      String[] exceptions) {
    super(access, name, desc, signature, exceptions);
    this.checkAccessModifiers();
  }

  public ComparisonMethodNode(int api) {
    super(api);
    this.checkAccessModifiers();
  }
  
  private void checkAccessModifiers() {
    this.isAbstract = (this.access & ACC_ABSTRACT) != 0;
    this.isFinal = (this.access & ACC_FINAL) != 0;
    this.isStatic = (this.access & ACC_STATIC) != 0;
    this.isPublic = (this.access & ACC_PUBLIC) != 0;
    this.isPrivate = (this.access & ACC_PRIVATE) != 0;
    this.isProtected = (this.access & ACC_PROTECTED) != 0;
    this.isStrict = (this.access & ACC_STRICT) != 0;
    this.isNative = (this.access & ACC_NATIVE) != 0;
    this.isSynchronized = (this.access & ACC_SYNCHRONIZED) != 0;
    this.isSynthetic = (this.access & ACC_SYNTHETIC) != 0;
    this.isTransient = (this.access & ACC_TRANSIENT) != 0;
  }


  @Override
  public void visitLdcInsn(Object cst) {
    this.ldcValues.add(cst);
    if (cst instanceof String) {
      this.ldcStringValues.add((String) cst);
    } else if (cst instanceof Float) {
      this.ldcFloatValues.add((Float) cst);
    } else if (cst instanceof Integer) {
      this.ldcIntValues.add((Integer) cst);
    } else if (cst instanceof Double) {
      this.ldcDoubleValues.add((Double) cst);
    } else if (cst instanceof Long) {
      this.ldcLongValues.add((Long) cst);
    } else if (cst instanceof Type) {
      Type t = (Type) cst;
      int sort = t.getSort();
      if (sort == Type.OBJECT) {
        this.ldcObjectTypeValues.add(t);
      } else if (sort == Type.ARRAY) {
        this.ldcArrayTypeValues.add(t);
      } else if (sort == Type.METHOD) {
        this.ldcMethodTypeValues.add(t);
      }
    } else if (cst instanceof Handle) {
      this.ldcHandleValues.add((Handle)cst);
    }
    super.visitLdcInsn(cst);
  }

  public double compareTo(ComparisonMethodNode other) {
    if (other.access == this.access) {
      return 1.0;
    }
    int score = 0;
    int numModifiers = getModifierCount(this);
    for (int i : possibleBitMasks) {
      if (((this.access & i) != 0) == ((other.access & i) != 0)) {
        score++;
      }
    }
    return ((double)score / numModifiers);
  }
  
  /**
   * Counts how many modifiers a method has.
   * @param mn the {@link MethodNode} whose modifiers are being checked.
   * @return the number of modifiers used for the method.
   */
  public static int getModifierCount(MethodNode mn) {
    return countBitMasksUsed(possibleBitMasks, mn.access);
  }
  private static int countBitMasksUsed(int[] masksPossible, int flags) {
    int total = 0;
    for (int i : masksPossible) {
      if ((flags & i) != 0) {
        total++;
      }
    }
    return total;
  }
  
  
  public double getComparisonScoreToClosestMethod(ComparisonMethodNode node) {
    return 1.0;
  }
}
