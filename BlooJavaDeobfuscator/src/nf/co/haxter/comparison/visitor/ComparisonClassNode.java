package nf.co.haxter.comparison.visitor;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;

public class ComparisonClassNode extends ClassNode implements Opcodes {
  public List<ComparisonMethodNode> methodComparisons = new ArrayList<ComparisonMethodNode>();
  
  public boolean isStatic;
  public boolean isPublic;
  public boolean isPrivate;
  public boolean isProtected;
  public boolean isAbstract;
  public boolean isFinal;
  public boolean isStrict;

  
  
  @Override
  public void visit(int version, int access, String name, String signature, String superName,
      String[] interfaces) {
    super.visit(version, access, name, signature, superName, interfaces);
    this.isAbstract = (access & ACC_ABSTRACT) != 0;
    this.isFinal = (access & ACC_FINAL) != 0;
    this.isPrivate = (access & ACC_PRIVATE) != 0;
    this.isPublic = (access & ACC_PUBLIC) != 0;
    this.isProtected = (access & ACC_PROTECTED) != 0;
    this.isStatic = (access & ACC_STATIC) != 0;
    this.isStrict = (access & ACC_STRICT) != 0;
  }



  @Override
  public MethodVisitor visitMethod(int access, String name, String desc, String signature,
      String[] exceptions) {
    ComparisonMethodNode cmn = new ComparisonMethodNode();
    cmn.accept(super.visitMethod(access, name, desc, signature, exceptions));
    this.methodComparisons.add(cmn);
    return cmn;
  }
  
  
}
