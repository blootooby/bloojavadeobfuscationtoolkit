package nf.co.haxter.comparison.visitor;

import nf.co.haxter.comparison.util.StringComparisonUtils;

public class ClassComparer implements IClassComparer {
  public boolean compareModifiers = true;
  public boolean compareDescriptors = true;

  @Override
  public double getComparisonScore(ComparisonClassNode node1, ComparisonClassNode node2) {
    // TODO comparing methods, field, annotations, interfaces, superclass (might need to do extra
    // work on superclass).
    double scoreMethods = 1;
    
    return 0;
  }

  @Override
  public double getComparisonScore(ComparisonFieldNode node1, ComparisonFieldNode node2) {
    // TODO comparing descriptors, modifiers, annotations.
    return 0;
  }

  @Override
  public double getComparisonScore(ComparisonMethodNode node1, ComparisonMethodNode node2) {
    // TODO comparing annotations, instructions.
    double scoreModifier = this.compareModifiers ? 1.0 : node1.compareTo(node2);
    double scoreDescriptor =
        this.compareDescriptors ? 1.0 : StringComparisonUtils.getCombinedScore(node1.desc,
            node2.desc);
    
    return scoreModifier * scoreDescriptor;
  }
}
