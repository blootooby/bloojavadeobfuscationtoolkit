package nf.co.haxter.comparison.visitor;

public interface IClassComparer {
  public double getComparisonScore(ComparisonClassNode node1, ComparisonClassNode node2);
  public double getComparisonScore(ComparisonFieldNode node1, ComparisonFieldNode node2);
  public double getComparisonScore(ComparisonMethodNode node1, ComparisonMethodNode node2);
}
