package nf.co.haxter.comparison.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;

import nf.co.haxter.exceptions.ComparisonException;
import nf.co.haxter.util.InsnPrinter;

import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.LineNumberNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.TypeInsnNode;

/**
 * A utility class for making comparisons.
 * 
 * @author blootooby
 * @see July 29, 2014.
 */
public final class ClassComparisonUtils {
  /**
   * Makes a list of all equal values in the provided lists.
   * 
   * @param list1 a {@link List} of values.
   * @param lists {@link List}s to compare to list1.
   * @return a list of all equal values in the provided lists.
   */
  public static List<Object> getEqualValues(List<Object> list1, List<Object>... lists) {
    if (lists.length == 0) {
      return list1;
    }
    List<Object> ret = new ArrayList<Object>();
    for (Object o : list1) {
      boolean contained = true;
      for (List<Object> list : lists) {
        if (!list.contains(o)) {
          contained = false;
          break;
        }
      }
      if (contained) {
        ret.add(o);
      }
    }
    return ret;
  }

  /**
   * Creates a map of instruction indexes (in the larger list, or the second if they are equal in
   * size) to similar instruction nodes.
   * 
   * @param list1 first list
   * @param list2 second list
   * @return a map of similar instruction indexes.
   */
  public static Map<Integer, AbstractInsnNode> getSimilarInstructions(List<AbstractInsnNode> list1,
      List<AbstractInsnNode> list2) {
    Map<Integer, AbstractInsnNode> ret = new HashMap<Integer, AbstractInsnNode>();
    List<AbstractInsnNode> larger = (list1.size() > list2.size() ? list1 : list2);
    List<AbstractInsnNode> smaller = (larger == list1 ? list2 : list1);
    List<AbstractInsnNode> used = new ArrayList<AbstractInsnNode>();

    for (int i = 0; i < larger.size(); i++) {
      AbstractInsnNode largerNode = larger.get(i);
      for (AbstractInsnNode ain : smaller) {
        if (used.contains(ain)) {
          continue;
        }
        if (largerNode == ain || runNodeCheck(largerNode, ain, null, true, true)) {
          ret.put(i, largerNode);
          used.add(ain);
          break;
        }
      }
    }
    return ret;
  }

  public static Map<LineNumberMapping, Integer> getLineSimilarityMap(List<AbstractInsnNode> list1,
      List<AbstractInsnNode> list2, boolean doTypeChecking,
      boolean ignoreLabelNodesAndLineNumberNodes) {
    Map<Integer, AbstractInsnNode> ret = new HashMap<Integer, AbstractInsnNode>();
    List<List<AbstractInsnNode>> lines1 = separateByLine(list1, LabelNode.class);
    List<List<AbstractInsnNode>> lines2 = separateByLine(list2, LabelNode.class);

    Map<LineNumberMapping, Integer> lineSimilarityMap = new HashMap<LineNumberMapping, Integer>();
    for (int i = 0; i < lines1.size(); i++) {
      List<AbstractInsnNode> xList1 = lines1.get(i);
      for (int i1 = 0; i1 < lines2.size(); i1++) {
        List<AbstractInsnNode> xList2 = lines2.get(i1);
        float simPoints = 0;
        int total = xList1.size();
        List<AbstractInsnNode> used = new ArrayList<AbstractInsnNode>();
        for (AbstractInsnNode l : xList1) {
          if (ignoreLabelNodesAndLineNumberNodes && l instanceof LineNumberNode
              || l instanceof LabelNode) {
            total--;
            continue;
          }
          for (AbstractInsnNode s : xList2) {
            if (!used.contains(s)
                && (l == s || (l != null && s != null && runNodeCheck(l, s, null, doTypeChecking,
                    true)))) {
              simPoints++;
              used.add(s);
              break;
            }
          }
        }
        if (xList1.size() < xList2.size()) {
          simPoints *= .5;
        }
        
        lineSimilarityMap.put(new LineNumberMapping(i, i1),
            (int) ((simPoints / total) * 100));
      }
    }

    return lineSimilarityMap;
  }

  /**
   * Creates a map of instruction indexes (in the larger list, or the second if they are equal in
   * size) to similar instruction nodes.
   * 
   * @param list1 first list
   * @param list2 second list
   * @return a map of similar instruction indexes.
   */
  public static Map<Integer, AbstractInsnNode> getSimilarInstructions(InsnList list1, InsnList list2) {
    return getSimilarInstructions(toJavaList(list1), toJavaList(list2));
  }

  /**
   * Returns the line number most similar to the line number provided.
   * 
   * @param method1 the original method.
   * @param method2 the method to be checked.
   * @param originalLine the line in the original method to look for.
   * @param typeChecking whether to check types (in descriptors, owner names, signatures, etc.) when
   *        finding line similarities)
   * @return the line number in the second method most similar to the one provided.
   */
  public static int getMostSimilarLine(MethodNode method1, MethodNode method2, int originalLine,
      boolean typeChecking) throws ComparisonException {
    Entry<LineNumberMapping, Integer> closest = null;
    Map<LineNumberMapping, Integer> lsm =
        getLineSimilarityMap(toJavaList(method1.instructions), toJavaList(method2.instructions),
            typeChecking, true);
    for (Entry<LineNumberMapping, Integer> e : lsm.entrySet()) {
      if (closest == null || closest.getValue() < e.getValue()) {
        closest = e;
      }
    }
    if (closest == null) {
      throw new ComparisonException("Could not find any lines!");
    }
    return closest.getKey().line1;
  }

  /**
   * Separates a "wall" of instructions into a {@link List} of smaller instruction lists, separated
   * by a specified node type.
   * 
   * @param instructions the instructions to separate.
   * @param nodeToSeparateBy the node type to separate by.
   * @return a list of smaller instruction lists, separated by the specified node type.
   */
  public static List<List<AbstractInsnNode>> separateByLine(List<AbstractInsnNode> instructions, Class<? extends AbstractInsnNode> nodeToSeparateBy) {
    List<List<AbstractInsnNode>> lines = new ArrayList<List<AbstractInsnNode>>();

    ListIterator<AbstractInsnNode> it = instructions.listIterator();
    List<AbstractInsnNode> insns = new ArrayList<AbstractInsnNode>();
    boolean foundLineNum = false;
    while (it.hasNext()) {
      AbstractInsnNode ain = it.next();
      if (ain instanceof LineNumberNode) {
        if (foundLineNum) {
          lines.add(insns);
          it.previous();
          foundLineNum = false;
          continue;
        } else {
          insns = new ArrayList<AbstractInsnNode>();
          insns.add(ain);
          foundLineNum = true;
          continue;
        }
      }
      if (foundLineNum) {
        insns.add(ain);
      }
    }
    return lines;
  }

  public static boolean runNodeCheck(AbstractInsnNode ain1, AbstractInsnNode ain2,
      Class<?>[] ignoreNodes, boolean typeChecks, boolean ldcChecks) {
    if (ignoreNodes != null) {
      for (Class<?> ignoredNodeType : ignoreNodes) {
        if (ain1.getClass().equals(ignoredNodeType) || ain2.getClass().equals(ignoredNodeType)) {
          return true;
        }
      }
    }


    boolean opcodeCheck = ain1.getOpcode() == ain2.getOpcode();
    boolean typeCheck = ain1.getType() == ain2.getType();
    if (!opcodeCheck || !typeCheck) {
      return false;
    }
    if (ldcChecks && ain1 instanceof LdcInsnNode) {
      LdcInsnNode lin1 = (LdcInsnNode) ain1;
      LdcInsnNode lin2 = (LdcInsnNode) ain2;
      if (!((lin1.cst == lin2.cst) || (lin1.cst != null && lin1.cst.equals(lin2.cst)))) {
        return false;
      }
      return true;
    }

    if (typeChecks) {
      if (ain1 instanceof TypeInsnNode) {
        TypeInsnNode lin1 = (TypeInsnNode) ain1;
        TypeInsnNode lin2 = (TypeInsnNode) ain2;

        if (!strCheck(lin1.desc, lin2.desc)) {
          return false;
        }
      } else if (ain1 instanceof FieldInsnNode) {
        FieldInsnNode fin1 = (FieldInsnNode) ain1;
        FieldInsnNode fin2 = (FieldInsnNode) ain2;
        if (!strCheck(fin1.desc, fin2.desc) || !strCheck(fin1.owner, fin2.owner)) {
          return false;
        }
      } else if (ain1 instanceof MethodInsnNode) {
        MethodInsnNode min1 = (MethodInsnNode) ain1;
        MethodInsnNode min2 = (MethodInsnNode) ain2;
        if (!strCheck(min1.desc, min2.desc) || !strCheck(min1.owner, min2.owner)) {
          return false;
        }
      }
    }

    return true;
  }

  private static boolean strCheck(String x0, String x1) {
    return x0 == x1 || (x0 != null && x0.equals(x1));
  }

  /**
   * Converts an {@link InsnList} to a {@link List}.
   * 
   * @param list the {@link InsnList} to convert.
   * @return the {@link List}
   */
  public static List<AbstractInsnNode> toJavaList(InsnList list) {
    List<AbstractInsnNode> l = new ArrayList<AbstractInsnNode>();
    for (int i = 0; i < list.size(); i++) {
      l.add(list.get(i));
    }
    return l;
  }

  public static class LineNumberMapping {
    private final int line1;
    private final int line2;

    public LineNumberMapping(int line1, int line2) {
      this.line1 = line1;
      this.line2 = line2;
    }

    public int getLine1() {
      return line1;
    }

    public int getLine2() {
      return line2;
    }
  }
}
