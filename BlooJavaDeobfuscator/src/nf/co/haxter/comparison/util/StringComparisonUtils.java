package nf.co.haxter.comparison.util;

public final class StringComparisonUtils {
  public static double getLevenshteinScore(String s1, String s2) {
    return LevenshteinDistance.similarity(s1, s2);
  }

  public static double getJaroScore(String s1, String s2) {
    return JaroWinkler.similarity(s1, s2);
  }

  public static double getCombinedScore(String s1, String s2) {
    return getJaroScore(s1, s2) * getLevenshteinScore(s1, s2);
  }

}


/**
 * @author acdcjunior on StackOverflow.com
 */
class LevenshteinDistance {

  public static double similarity(String s1, String s2) {
    if (s1.length() < s2.length()) {
      String swap = s1;
      s1 = s2;
      s2 = swap;
    }
    int bigLen = s1.length();
    if (bigLen == 0) {
      return 1.0;
    }
    return (bigLen - computeEditDistance(s1, s2)) / (double) bigLen;
  }

  public static int computeEditDistance(String s1, String s2) {
    s1 = s1.toLowerCase();
    s2 = s2.toLowerCase();

    int[] costs = new int[s2.length() + 1];
    for (int i = 0; i <= s1.length(); i++) {
      int lastValue = i;
      for (int j = 0; j <= s2.length(); j++) {
        if (i == 0)
          costs[j] = j;
        else {
          if (j > 0) {
            int newValue = costs[j - 1];
            if (s1.charAt(i - 1) != s2.charAt(j - 1))
              newValue = Math.min(Math.min(newValue, lastValue), costs[j]) + 1;
            costs[j - 1] = lastValue;
            lastValue = newValue;
          }
        }
      }
      if (i > 0)
        costs[s2.length()] = lastValue;
    }
    return costs[s2.length()];
  }
}


/**
 * origin:
 * https://code.google.com/p/duke/source/browse/src/main/java/no/priv/garshol/duke/JaroWinkler
 * .java?r=e32a5712dbd51f1d4c81e84cfa438468e217a65d
 */
class JaroWinkler {

  /**
   * Returns normalized score, with 0.0 meaning no similarity at all, and 1.0 meaning full equality.
   */
  public static double similarity(String s1, String s2) {
    if (s1.equals(s2))
      return 1.0;

    if (s1.length() > s2.length()) {
      String tmp = s2;
      s2 = s1;
      s1 = tmp;
    }
    int maxdist = s2.length() / 2;
    int c = 0;
    int t = 0;
    int prevpos = -1;
    for (int ix = 0; ix < s1.length(); ix++) {
      char ch = s1.charAt(ix);
      for (int ix2 = Math.max(0, ix - maxdist); ix2 < Math.min(s2.length(), ix + maxdist); ix2++) {
        if (ch == s2.charAt(ix2)) {
          c++;
          if (prevpos != -1 && ix2 < prevpos)
            t++;
          prevpos = ix2;
          break;
        }
      }
    }

    if (c == 0)
      return 0.0;

    double score =
        ((c / (double) s1.length()) + (c / (double) s2.length()) + ((c - t) / (double) c)) / 3.0;

    int p = 0;
    int last = Math.min(4, s1.length());
    for (; p < last && s1.charAt(p) == s2.charAt(p); p++);

    score = score + ((p * (1 - score)) / 10);

    return score;
  }

}
