package nf.co.haxter.exceptions;

public class ComparisonException extends Exception {

  public ComparisonException(String message) {
    super(message);
  }
  
}
