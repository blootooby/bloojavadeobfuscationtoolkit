package nf.co.haxter.util;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.util.Printer;
import org.objectweb.asm.util.Textifier;
import org.objectweb.asm.util.TraceMethodVisitor;

/**
 * @author Jonas Berlin from stackoverflow.com
 * <br>
 * http://stackoverflow.com/questions/18028093/java-asm-tree-api-how-to-pretttyprint-abstractinstructionnode
 */
public class InsnPrinter {
  private static final Printer printer = new Textifier();
  private static final TraceMethodVisitor methodPrinter = new TraceMethodVisitor(printer);

  public static String prettyprint(AbstractInsnNode insnNode) {
    insnNode.accept(methodPrinter);
    StringWriter sw = new StringWriter();
    printer.print(new PrintWriter(sw));
    printer.getText().clear();
    return sw.toString();
  }
}