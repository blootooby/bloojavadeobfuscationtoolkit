package nf.co.haxter.util;

import java.lang.reflect.Method;

public final class BytecodeUtils {
  public static String translateTypeToInternalType(String s) {
    /*
     * byte[]
     * long[]
     * 
     * Object[]
     * 
     * java.util.Map<x, y>
     */
    
    if ("void".equals(s)) {
      return "V";
    } else if (s.endsWith("[]")) {
      s = s.substring(0, s.length() - 2);
      s = '[' + translateTypeToInternalType(s) + ';';
    } else if ("byte".equals(s)) {
      s = "B";
    } else if ("boolean".equals(s)) {
      s = "Z";
    } else if ("char".equals(s)) {
      s = "C";
    } else if ("double".equals(s)) {
      s = "D";
    } else if ("float".equals(s)) {
      s = "F";
    } else if ("int".equals(s)) {
      s = "I";
    } else if ("long".equals(s)) {
      s = "J";
    } else if ("short".equals(s)) {
      s = "S";
    } else {
      if (s.startsWith("class ")) {
        s = s.substring(6);
      }
      s = 'L' + s.replace('.', '/') + ';';
    }
    
    return s;
  }
  
  public static String getPackageName(String fullyQualifiedClassName) {
    int i = fullyQualifiedClassName.lastIndexOf('.');
    if (i == -1) {
      return "";
    }
    return fullyQualifiedClassName.substring(0, i);
  }
  
  public static String getClassName(String fullyQualifiedClassName) {
    return fullyQualifiedClassName.substring(fullyQualifiedClassName.lastIndexOf('.') + 1);
  }
  
  /**
   * Checks if the passed-in byte array begins with 0xCAFEBABE.
   * @param bytecode the byte array to check.
   * @return if the magic number was found.
   */
  public static boolean hasMagic(byte[] bytecode) {
    return checkMagic(bytecode, toByteArray(new int[] {0xCA, 0xFE, 0xBA, 0xBE}));
  }
  
  private static boolean checkMagic(byte[] b, byte[] magic) {
    if (b.length < magic.length) {
      return false;
    }
    for (int i = 0; i < magic.length; i++) {
      if ((b[i] & 0xFF) != magic[i]) {
        return false;
      }
    }
    return true;
  }
  
  public static byte[] toByteArray(int[] unsigned) {
    byte[] b = new byte[unsigned.length];
    for (int i = 0; i < unsigned.length; i++) {
      b[i] = (byte)unsigned[i];
    }
    return b;
  }
  
  public static String getMethodSignature(Method method) {
    Class<?>[] paramTypes = method.getParameterTypes();
    String signature = "(";
    for (Class<?> c : paramTypes) {
      String ret = formatParameter(c);
      if (ret.charAt(0) == 'L') {
        ret += ';';
      }
      signature += ret;
    }
    String ret = formatParameter(method.getReturnType());
    if (ret.charAt(0) == 'L') {
      ret += ';';
    }
    return signature
        + ')' + ret;
  }

  private static String formatParameter(Class<?> paramType) {
    String r = "";
    if (paramType.equals(Boolean.TYPE)) {
      r += 'Z';
    } else if (paramType.equals(Byte.TYPE)) {
      r += 'B';
    } else if (paramType.equals(Character.TYPE)) {
      r += 'C';
    } else if (paramType.equals(Double.TYPE)) {
      r += 'D';
    } else if (paramType.equals(Float.TYPE)) {
      r += 'F';
    } else if (paramType.equals(Integer.TYPE)) {
      r += 'I';
    } else if (paramType.equals(Long.TYPE)) {
      r += 'J';
    } else if (paramType.equals(Short.TYPE)) {
      r += 'S';
    } else if (paramType.equals(Void.TYPE)) {
      r += 'V';
    } else if (paramType.isArray()) {
      r += paramType.toString().substring(6).replace('.', '/');
    } else {
      r += 'L' + paramType.getName().replace('.', '/');
    }
    return r;
  }
}
