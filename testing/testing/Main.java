package testing;

import nf.co.haxter.reflected.HookFactory;
import testing.hooks.TestClass2Hook;

public class Main {
  public static void main(String[] strings) throws Exception {
    /*ClassReader cr = new ClassReader(new FileInputStream(new File(strings[0])));
    ClassReader cr2 = new ClassReader(new FileInputStream(new File(strings[1])));
    ClassNode cn = new ClassNode();
    cr.accept(cn, 0);
    MethodNode mn1 = MappingUtils.getMethodNode(cn, "postLoad", "(Lcpw/mods/fml/common/event/FMLPostInitializationEvent;)V");
    cn = new ClassNode();
    cr2.accept(cn, 0);
    MethodNode mn2 = MappingUtils.getMethodNode(cn, "postLoad", "(Lcpw/mods/fml/common/event/FMLPostInitializationEvent;)V");

    Map<Integer, AbstractInsnNode> sims =
        ClassComparisonUtils.getSimilarInstructions(mn1.instructions, mn2.instructions);

    System.out.println("Listing similarities: ");
    for (Entry<Integer, AbstractInsnNode> e : sims.entrySet()) {
      int type = e.getValue().getType();
      if (type == AbstractInsnNode.LABEL || type == AbstractInsnNode.LINE) {
        continue;
      }
      System.out.println("Index: " + e.getKey().toString() + ", Instruction: "
          + InsnPrinter.prettyprint(e.getValue()));
    }

    System.out.println("Dumping similarity map: ");
    Map<LineNumberMapping, Integer> lsm =
        ClassComparisonUtils.getLineSimilarityMap(
            ClassComparisonUtils.toJavaList(mn1.instructions),
            ClassComparisonUtils.toJavaList(mn2.instructions), false, true);
    for (Entry<LineNumberMapping, Integer> e : lsm.entrySet()) {
      LineNumberMapping mapping = e.getKey();
      if (e.getValue() < 50) {
        continue;
      }
      System.out.println(String.format("%s -> %s (%s)", mapping.getLine1(), mapping.getLine2(), e
          .getValue().toString()));
    }

    //System.out.println("Most similar line: "
    //    + ClassComparisonUtils.getMostSimilarLine(mn1, mn2, 0, true));
     * */
    
    
    
    
    HookFactory.getDefault().createHook(TestClass2.class, TestClass2Hook.class);
    TestClass2Hook hook = HookFactory.getDefault().getHook(TestClass2Hook.class);
    System.out.println(hook.getX());
    hook.run();
    hook.testMethod1();
  }
}
