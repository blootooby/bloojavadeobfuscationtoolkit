package testing;

import java.util.Scanner;

public class TestClass1 {
  public static int x0 = 0;
  
  public static void run() {
    for (int i = 0; i < 500; i++) {
      x0 += i;
      x0 -= i * 2;
    }
    
    Scanner scanner = new Scanner(System.in);
    scanner.close();
    System.out.println("I like chocolate");
    System.out.println("I like apples");
    System.out.println("I do not like apples");
    System.out.println();
  }
}
