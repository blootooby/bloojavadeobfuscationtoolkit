package testing.hooks;

import nf.co.haxter.reflected.interfacer.CustomMethod;
import nf.co.haxter.reflected.interfacer.ExistingMethod;
import nf.co.haxter.reflected.interfacer.Getter;
import nf.co.haxter.reflected.interfacer.Hook;
import nf.co.haxter.reflected.interfacer.Reflected;


@Reflected(className = "testing.TestClass2", isObfuscated = false)
public interface TestClass2Hook extends Hook {

  @Getter(isObfuscated = false, fieldName = "x")
  public int getX();

  @ExistingMethod(isObfuscated = false, isStatic = true, name = "run", descriptor = "()V")
  public void run();


  @CustomMethod(codeDefinition = testMethod1Body.class)
  public void testMethod1();

  public static final class testMethod1Body implements Runnable {
    @Override
    public void run() {
      System.out.println("TestClass2Hook.testMethod1Body.run()");
    }
  }

}
